module AdminsHelper

  def file_extension
      file_content_type.split("/").last.to_sym
  end
end
