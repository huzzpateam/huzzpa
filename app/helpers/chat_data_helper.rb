module ChatDataHelper
  def get_message(message_id)
     Message.find(message_id).body
  end
end
