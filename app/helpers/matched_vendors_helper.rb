module MatchedVendorsHelper
  def get_chat_access(vendor_id)
     ChatAccess.where( user_id:  current_user.id , vendor_profile_id: vendor_id)
  end
end
