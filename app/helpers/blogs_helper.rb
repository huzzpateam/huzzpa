module BlogsHelper
  def get_category_name(category_id)
     Category.find(category_id).name
  end
  def get_type_name(type_id)
    if type_id.nil?
      "NA"
    else
      Type.find(type_id).name
    end
  end

  def get_typesin_category(category_id)
    Type.where(category_id: category_id)
  end

  # def is_blog_url?
  #   # puts "-------#{Rails.application.routes.recognize_path(request.path)}--------"
  #   # # extracted_path = Rails.application.routes.recognize_path(request.path)
  #   # # extracted_path[:controller] == "blogs"
  # end
end
