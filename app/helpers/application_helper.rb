module ApplicationHelper
  def active_class(link_path)
    current_page?(link_path) ? "tab-button-active" : ""
  end

  def get_conversation(user)
    Conversation.where('sender_id= ? OR recipient_id=?',user, user)
  end

  def present_url(url)
    url == root_url || url == about_url || url == interior_designers_mumbai_url || url == new_user_session_url ||
      url == new_user_registration_url || url == faq_url || url == privacy_url ||
      url == terms_url
  end

  def chat_visible?(user)
    if user
      if user.try(:role) == "user"  && !user.guest
        return (ChatAccess.where(user_id: user , status: true).count > 0)
      elsif user.try(:role) == "vendor"
        return (ChatAccess.where(vendor_profile_id: user.vendor_profile , status: true ).count > 0)
      end
    else
      return false
    end
  end
end
