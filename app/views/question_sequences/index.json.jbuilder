json.array!(@question_sequences) do |question_sequence|
  json.extract! question_sequence, :id
  json.url question_sequence_url(question_sequence, format: :json)
end
