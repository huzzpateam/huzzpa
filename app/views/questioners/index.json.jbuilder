json.array!(@questioners) do |questioner|
  json.extract! questioner, :id
  json.url questioner_url(questioner, format: :json)
end
