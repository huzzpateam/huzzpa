json.array!(@chat_accesses) do |chat_access|
  json.extract! chat_access, :id
  json.url chat_access_url(chat_access, format: :json)
end
