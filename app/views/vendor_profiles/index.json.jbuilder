json.array!(@vendor_profiles) do |vendor_profile|
  json.extract! vendor_profile, :id, :company, :about, :address, :vendor_id
  json.url vendor_profile_url(vendor_profile, format: :json)
end
