json.extract! fav_product, :id, :created_at, :updated_at
json.url fav_product_url(fav_product, format: :json)