json.extract! huzzpa_work, :id, :created_at, :updated_at
json.url huzzpa_work_url(huzzpa_work, format: :json)