json.array!(@vendor_works) do |vendor_work|
  json.extract! vendor_work, :id, :image_id, :description, :price, :vendor_id
  json.url vendor_work_url(vendor_work, format: :json)
end
