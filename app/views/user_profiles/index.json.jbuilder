json.array!(@user_profiles) do |user_profile|
  json.extract! user_profile, :id, :location, :work_details, :profile_image_id, :preference_no, :user_id
  json.url user_profile_url(user_profile, format: :json)
end
