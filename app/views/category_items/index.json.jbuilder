json.array!(@category_items) do |category_item|
  json.extract! category_item, :id, :description, :tags, :type_id, :category_id
  json.image "#{request.base_url}#{attachment_url(category_item, :image, :fill, 300, 300, format: 'jpg')}"
  json.url category_item_url(category_item, format: :json)
end
