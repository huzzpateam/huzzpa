json.extract! @category_item, :id, :description, :tags, :type_id, :category_id, :created_at, :updated_at
json.image "#{request.base_url}#{attachment_url(category_item, :image, :fill, 300, 300, format: 'jpg')}"
