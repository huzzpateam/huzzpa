class UserMailer < ApplicationMailer

  def connect(user)
    @user = user
    mail(subject: "Thank you for contacting us!!", to: "#{user.email}", from: "hello@huzzpa.com")
  end

  def ack(user)
    @user = user
    mail(subject: "Thank you for contacting us!!", to: "#{user.email}", from: "hello@huzzpa.com")
  end
end
