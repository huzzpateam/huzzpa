class DeviseInstructionMailer < Devise::Mailer
  helper :application # gives access to all helpers defined within `application_helper`.
  include Devise::Controllers::UrlHelpers # Optional. eg. `confirmation_url`
  default template_path: 'devise/mailer' # to make sure that you mailer uses the devise views

  def confirmation_instructions(record, token, opts={})
    opts[:subject] = "Welcome to Huzzpa! Please confirm your account"
    super
  end

  def reset_password_instructions(record, token, opts={})
    opts[:subject] = "Huzzpa.com Password Assistance"
    super
  end
end
