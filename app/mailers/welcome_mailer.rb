class WelcomeMailer < ApplicationMailer

  def welcome_new_user(user)
    @user = user
    mail(subject: "Welcome to Huzzpa!! #{user.name}", to: "#{user.email}", from: "hello@huzzpa.com")
  end
end
