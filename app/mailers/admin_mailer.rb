class AdminMailer < ApplicationMailer
  default from: "hello@huzzpa.com"
  default to: "hello@huzzpa.com"

  def new_user(user)
    @user = user
    if user.role == 'user' then @subject = "New user has signed up" elsif user.role == 'vendor' then @subject = "New Professional has signed up." end
    mail(subject: "#{@subject}: #{user.email}")
  end

  def connect_vendor(user, vendor)
    @user = user
    @vendor = vendor
    mail(subject: "User #{user.name} wants to contact Professional #{vendor.name}")
  end

  def connect_user(vendor, user)
    @user = user
    @vendor = vendor
    mail(subject: "Professional #{vendor.name} wants to contact user #{user.name}")
  end

  def activate_me(vendor)
    @vendor = vendor
    mail(subject: "Professional #{vendor.name} wants to activate his/her account.")
  end

  def error(user)
    @user = user
    mail(subject: "Somthing went wrong while saving this user: #{user.email}")
  end

  def subscribe(email)
    @email = email
    mail(subject: "New Subscriber with email-id: #{email}")
  end

  def ack_subscribe(email)
    @email = email
    mail(to: @email, subject: "Thank You For Subscribing to Huzzpa !!")
  end

  def vendor_status_update(vendor)
    @vendor = vendor
    mail(subject: "Hello #{@vendor.name}, Your acount status on Huzzpa has been updated!!")
  end

  def approve_chat(user, vendor)
    mail(to: user.email ,subject: "Hello #{@vendor.name}, Your Chat Acccess request with our professional #{vendor.name} is approved on Huzzpa !!")
    mail(to: vendor.email ,subject: "Hello #{@vendor.name}, Your are allowed to chat with #{user.name} on Huzzpa !!")
  end

  # def mandrill_client
  #   @mandrill_client ||= Mandrill::API.new MANDRILL_API_KEY
  # end

  # def connect(user, vendor)
  #   template_name = "connect-user"
  #   template_content = []
  #   message = {
  #     to: [{email: 'hello@huzzpa.com'}],
  #     subject: "User #{user.name} wants to Connect with Professional #{vendor.name}",
  #     merge_vars: [
  #       {rcpt: 'hello@huzzpa.com',
  #        vars: [
  #          {name: "USER_NAME", content: user.name},
  #          {name: "USER_PHONE_NUMBER", content: user.phone},
  #          {name: "USER_EMAIL_ID", content: user.email},
  #          {name: "VENDOR_NAME", content: vendor.name},
  #          {name: "VENDOR_PHONE_NUMBER", content: vendor.phone},
  #          {name: "VENDOR_EMAIL_ID", content: vendor.email}
  #        ]
  #       }
  #     ]
  #   }
  #   mandrill_client.messages.send_template template_name, template_content, message
  # end

  # def new_user(user)
  #   template_name = "new-user-signup"
  #   template_content = []
  #   message = {
  #     to: [{email: 'hello@huzzpa.com'}],
  #     subject: "New user #{user.name}",
  #     merge_vars: [
  #       {rcpt: 'hello@huzzpa.com',
  #        vars: [
  #          {name: "USER_NAME", content: user.name},
  #          {name: "USER_PHONE_NUMBER", content: user.phone},
  #          {name: "USER_EMAIL_ID", content: user.email}
  #        ]
  #       }
  #     ]
  #   }
  #   mandrill_client.messages.send_template template_name, template_content, message
  # end
end
