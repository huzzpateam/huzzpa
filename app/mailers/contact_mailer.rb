class ContactMailer < ApplicationMailer
  # default to: "hello@huzzpa.com"

  def user_message(name, email, mobile, message)
    @name = name # for template
    @message = message
    @mobile = mobile
    @email = email
    mail(subject: "A message from, #{name}",to: "hello@huzzpa.com", from: 'hello@huzzpa.com')
  end

  def ack_message(name, email)
    mail(subject: "Thank you for contacting us!!, #{name}", to: "#{email}", from: 'hello@huzzpa.com')
  end
end
