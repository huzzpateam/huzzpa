class VendorMailer < ApplicationMailer

  def connect(user, vendor)
    @user = user
    @vendor = vendor
    mail(subject: "Congrates! Your profile was matched with a Huzzpa Customer.", to: "#{vendor.email}", from: "hello@huzzpa.com")
  end

  def ack(vendor)
    @vendor = vendor
    mail(subject: "Thank you for contacting us!!", to: "#{vendor.email}", from: "hello@huzzpa.com")
  end
end
