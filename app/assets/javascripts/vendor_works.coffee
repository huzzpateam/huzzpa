jQuery ->
  $(document).on "upload:start", "form", (e) ->
    $(this).find("input[type=submit]").attr "disabled", true
    $("#vendorWork").text("Uploading...")

  $(document).on "upload:progress", "form", (e) ->
    $("#vendorWork").text("please wait while image uploads....")
    # detail          = e.originalEvent.detail
    # console.log detail.loaded
    # percentComplete = Math.round(detail.loaded / detail.total * 100)
    # console.log percentComplete
    # $("#image").text("#{percentComplete}% uploaded")

  $(document).on "upload:success", "form", (e) ->
    $(this).find("input[type=submit]").removeAttr "disabled"  unless $(this).find("input.uploading").length
    $("#vendorWork").text("image looks good!! click save at the end")


$(document).on 'ready page:change', (event) ->
  $('.carousel').carousel()
  return
$(document).ready ready
$(document).on 'page:load', ready
