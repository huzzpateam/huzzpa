var readyAnalytics;
readyAnalytics = function() {
	if (window._gaq != null) {
		return _gaq.push(['_trackPageview']);
	}
	else if (window.pageTracker != null) {
		return pageTracker._trackPageview();
	}
};

// Loads javascript while loading page
$(document).ready(readyAnalytics);
$(document).on('page:load', readyAnalytics);
$(document).on('page:change', readyAnalytics);

var hotjarReady;
hotjarReady = (function(h,o,t,j,a,r){
		h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
		h._hjSettings={hjid:551455,hjsv:5};
		a=o.getElementsByTagName('head')[0];
		r=o.createElement('script');r.async=1;
		r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
		a.appendChild(r);
})(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');

// Loads javascript while loading page
$(document).ready(hotjarReady);
$(document).on('page:load', hotjarReady);
$(document).on('page:change', hotjarReady);
