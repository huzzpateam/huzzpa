var ready;
  ready = function() {
  var checked_no = 0;
  var deactiv_checked_no = 0;
  $(".updateSequence").click(function(){
    var ques_id = this.id;
    var seq_no = document.getElementById("seq."+ this.id).value ;

    $.ajax({
      type: "get",
      url: "/question_sequences/update_sequence",
      dataType: "json",
      data: {question_id: ques_id , sequence_no: seq_no },
      success: function(data){
           location.reload();
      },
      error: function (err){
      }
    });

  });


  $('.activate_ques').click(function() {

    if (this.checked) {
      checked_no++;
    }else {
      checked_no--;
    }
    if (checked_no > 0) {
      $('.add_ques_btn')[0].disabled = false
    }else {
      $('.add_ques_btn')[0].disabled = true
    }

  });

  $('.deactivate_ques').click(function() {

    if (this.checked) {
      checked_no++;
    }else {
      checked_no--;
    }
    if (checked_no > 0) {
      $('.rem_ques_btn')[0].disabled = false
    }else {
      $('.rem_ques_btn')[0].disabled = true
    }

  });
};

$(document).ready(ready);
$(document).on('page:load', ready); // Loads javascript while loading page
