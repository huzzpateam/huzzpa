var ready;
ready = function() {
  $( "#budget" ).change(function() {
     var amount = parseInt($(this).val());
     $('.filter-budget').hide();
     $('.filter-budget').each(function() {
       div = $(this);
       if(parseInt(div.data('budget')) <= amount ) {
         div.show('slow');
       }
     });
   });


// Filter in Recommendation page

   $('.categ_check').click(function() {

     var checkedCategIds = $('.categ_check:checked').map(function() {
      return this.value;
        }).get();
      var allCategIds = $('.categ_check').map(function() {
       return this.value;
         }).get();

      var allCategIds2 = $('.categ_check').map(function() {
          return this.value;
            }).get();
     while (allCategIds2.length) {
       $('.categ_id'+allCategIds2.pop()).show();
     }

      if (checkedCategIds.length > 0){
        while (checkedCategIds.length) {
          var id = checkedCategIds.pop();
          var index = allCategIds.indexOf(id);
          if (index > -1) {
            allCategIds.splice(index,1);
          }
        }
        while (allCategIds.length) {
          $('.categ_id'+allCategIds.pop()).hide('slow');
        }
      }else {
        while (allCategIds.length) {
          $('.categ_id'+allCategIds.pop()).show('slow');
        }
      }



        // if (checkedCategIds.length >= 0) {
        //   var product_ids = [];
        //   product_ids = $('.product_id').map(function() {
        //                   return this.dataset.productId;
        //                     }).get();
        //   $.ajax({
        //        type: "POST",
        //        url: "/matched_vendors/get_product_filter_by_categ",
        //        data: {category_id: checkedCategIds , id: product_ids},
        //        success: function(data) {
        //             $('#products_view').remove();
        //             $('#products_div').html(data);
        //        }
        //    });
        //
        // }

   });

// For price slider in recommendation engine
   $( "#slider-range" ).slider({
     range: true,
     min: 0,
     max: $('#max-price').data("max-price"),
     values: [ 0, $('#max-price').data("max-price") ],
     slide: function( event, ui ) {
       $( "#amount" ).val( "INR" + ui.values[ 0 ] + " - INR" + ui.values[ 1 ] );
       var min =  ui.values[ 0 ];
       var max =  ui.values[ 1 ];

       var price_arr = $('.price').map(function() {
          return this.dataset.price;
            }).get();


        while (price_arr.length) {
          var price = price_arr.pop();
          if (price >= min && price <= max) {
            var checkedCategIds = $('.categ_check:checked').map(function() {
           return this.value;
             }).get();

            if (checkedCategIds.length > 0) {
              var present = false;
              while (checkedCategIds.length) {
                var categ_id = checkedCategIds.pop();
                if ($('.price_'+price).hasClass("categ_id"+categ_id)) {
                    present = true;
                    break;
                }
              }
              if (present == true) {
                $('.price_'+price+".categ_id"+categ_id ).show('slow');
              }
            } else  {
                $('.price_'+ price ).show('slow');
            }
          }else {
            $('.price_'+ price ).hide('slow');
          }
        };
     }
   });
   $( "#amount" ).val( "INR" + $( "#slider-range" ).slider( "values", 0 ) +
     " - INR" + $( "#slider-range" ).slider( "values", 1 ) );

    $('.recommendation').click(function() {
      $('#choose_budget').hide();
    });
    $('.suggest_proff').click(function() {
      $('#choose_budget').show();
    });


 };

$(document).ready(ready);
$(document).on('page:load', ready); // Loads javascript while loading page

function set_favourate(product_id) {
  if ($('.fav'+product_id).hasClass("fa-heart-o")) {
    // Favourate icon is not liked
    $.ajax({
      type: "post",
      url: "/fav_products",
      dataType: "json",
      data: {product_id: product_id },
      success: function(data){
        $('.fav'+product_id).removeClass("fa-heart-o").addClass("fa-heart");
      },
      error: function (err){
      }
    });

  }
  else if ( $('.fav'+product_id).hasClass("fa-heart")) {
    $.ajax({
        type: "get",
        url: "/fav_products/remove_fav_prod",
        dataType: "json",
        data: {product_id: product_id},
        complete: function(){
            // Favourate icon is liked
            $('.fav'+product_id).removeClass("fa-heart").addClass("fa-heart-o");
        }
    });

  }
}

function chat_request_designer(vendor_id ) {
  $('#notice').html("");
  $.ajax({
      type: "post",
      url: "/matched_vendors/connect",
      dataType: "json",
      data: {vendor_id: vendor_id},
      complete: function(data){
            //  location.reload();
             $('#chataccessnotice').html("Thank you , We are super excited about your project !! Allow us 24 hours to connect with you.");
             document.getElementById("chat_request_btn"+vendor_id).innerHTML = "Requested";
             $("#chat_request_btn"+vendor_id).prop("disabled",true);
      }
  });
}
