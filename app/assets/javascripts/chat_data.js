
function populate_image(image_type) {
   var user_id = $('.user_name_nav li.active')[0].getAttribute('data-userid');
   var url = '/chat_data/get_chat_images?image_type=' + image_type + '&user_id='+user_id ;
  $.get(url, function(data) {
    $('#'+image_type).html(data);

    //Image popup
    $('.img-pop').on('click',function(){
        var sr=$(this).attr('src');
       $('#mimg').attr('src',sr);
       $('#myPopupModal').modal('show');
    });
  });
}

function reset_active() {
    $('#board_header li').removeClass("active");
    $('#board_content div').removeClass("active")
}

function get_project_phase(user_id) {
  var url = '/chat_data/get_project_phase?user_id='+user_id ;
  $.get(url, function(data) {
    $('#project_phase').html(data.project_phase + " Phase");
  });
}

function get_project_phase_for_dropdown(user_id) {
  var url = '/chat_data/get_project_phase?user_id='+user_id ;
  $.get(url, function(data) {
    $('#project_phase :selected').removeAttr("selected");
    // alert("jje");
    $('#project_phase option[value='+ data.project_phase_id+']').attr('selected','selected');
  });
}

function get_user_profile() {
  var user_id = $('.user_name_nav li.active')[0].getAttribute('data-userid');
  var url = '/chat_data/get_user_profile?user_id=' + user_id ;
 $.get(url, function(data) {
   $('#client').html(data); } ).done(function() {
    reinit_slick();
    //Image popup
    $('.img-pop').on('click',function(){
        var sr=$(this).attr('src');
       $('#mimg').attr('src',sr);
       $('#myPopupModal').modal('show');
    });
  });
}

function reinit_slick() {
  $('.user-profile-slider').slick({
  dots: false,
  infinite: true,
  speed: 1000,
  slidesToShow: 3,
  slidesToScroll: 1,
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1,
        infinite: true,
        dots: false
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
});
}

function updateChatStatus(ele , conversation_id) {
  var status_id = ele.value;
  if (confirm('Are you sure you want to Change the Project Phase ?')) {
    url = '/chat_data/update_conv_status?id='+conversation_id +'&status_id='+status_id  ;
    $.ajax({
      type: "get",
      url: url,
      // data: {id: conversation_id , status_id: status_id },
       dataType: "json",
      success:function(data) {
        console.log(data.notice);
        $('#mynotice').html("<center>"+data.notice+"</center>");
        setInterval(function(){  $('#mynotice').html("");},3000);
      }
    });
  }else {
    $('#mynotice').html("<center>Project Phase Not Updated!!</center>");
    setInterval(function(){  $('#mynotice').html("");},3000);
  }

}
