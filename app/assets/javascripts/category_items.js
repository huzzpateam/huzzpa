var ready;
ready = function() {
  $( ".category-item" ).change(function() {
   url = '/types?category_id=' + $(this).val();
   $.getJSON(url, function(data) {
     $('.category-type').html('<option value="">Select type</option>')
     for(i = 0; i < data.length; i++) {
       $('.category-type').append('<option value="' + data[i].id + '">' + data[i].name + '</option>' )
     }
   });
  });

  $( "#category" ).change(function() {
     var categoryName = $(this).val();
     if(categoryName === "all"){
        $('.filter-category').show('slow');
        return false;
     }
     $('.filter-category').hide();
     $('.filter-category').each(function() {
       div = $(this);
       if(div.data('category') === categoryName ) {
         div.show('slow');
       }
     });
   });
};// Loads javascript while loading page
$(document).ready(ready);
$(document).on('page:load', ready);
