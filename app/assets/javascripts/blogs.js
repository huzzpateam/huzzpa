function get_blogs(category_id) {
  $('.sidebar-item').removeClass('active');
  // $('.sidebar-sub-items').remove();

  $.ajax({
    type: "get",
    url: "/blogs/get_blogby_category",
    dataType: "html",
    data: {category_id: category_id },
    success: function(data){
        $('#all_blogs').empty();
        $('#all_blogs').html(data);
    },
    error: function (err){
    }
  });
}


function get_blogs_by_type(type_id) {
  $('.sidebar-item').removeClass('active');

  $.ajax({
    type: "get",
    url: "/blogs/get_blogby_type",
    dataType: "html",
    data: {type_id: type_id },
    success: function(data){
        $('#all_blogs').empty();
        $('#all_blogs').html(data);
    },
    error: function (err){
    }
  });
}

function get_latest_blogs() {
  $('.sidebar-item').removeClass('active');
  $.ajax({
    type: "get",
    url: "/blogs/get_latest_blogs",
    dataType: "html",
    data: { },
    success: function(data){
        $('#all_blogs').empty();
        $('#all_blogs').html(data);
    },
    error: function (err){
    }
  });
}

var ready;
ready = function() {
  $(".category-template").change(function(){
    url = '/blogs/template';
       $.get(url, function(data) {
         CKEDITOR.instances['blog_body'].setData(data);
       });
  });
     $('.reply-comment').click(function(){
       var commentId = $(this).attr('data-comment-id');
       $(this).hide();
       $("#comment-"+ commentId ).append('<textarea class="form-control comment_content" required="required" id= "comment_content-'+commentId+'"></textarea> <button name="button" class=" btn btn-sm reply-btn" id="reply-btn-'+commentId+'" data-id='+commentId+'  >Post</button>');
       $('.reply-btn').click(function(){
         var commentid = $(this).attr("data-id");
         var content = $("#comment_content-"+ commentid ).val();
         post_reply(commentid , content , this);
       });
     });

     function post_reply(commentId, content , ele) {
       var blog_id = $("#comment_blog_id").val();

       var url = '/comments';
       $.ajax({
          type: "POST",
          url: url,
          data: {comment: {blog_id: blog_id ,parent_id: commentId , content: content }},
          dataType: 'json',
          success: function(data){
            $('#comment_content-'+commentId).hide();
            $('#reply-btn-'+commentId).hide();
            $('#reply-comment-'+commentId).show();

            $.ajax({
              type: "Get",
              url: "/comments/reply",
              data: {comment: data },
              dataType: 'html',
              success: function(data){
                console.log(data);
                $('#well-'+commentId).append(data)
              }
            });
          }
        });
     }
 }

$(document).ready(ready);
$(document).on('page:load', ready); // Loads javascript while loading page
