var ready;
var storeData = [];
ready = function() {
  $(".category-slider img").tooltip();


// For slider in vendor preference page
$('.preference-slider').slick({
  dots: false,
  infinite: true,
  speed: 1000,
  slidesToShow: 4,
  slidesToScroll: 1,
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1,
        infinite: true,
        dots: false
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
});



$(".getNextQuestion").click(function(){
  var selectedImageId = [];
  var quesNo = [];
  $("input:checked").each(function() {
    selectedImageId.push($(this).val());
  });
  quesNo.push(document.getElementById("quesNo").value);
  storeData.push({question_id: quesNo , category_item_id: selectedImageId })

  // var curr_ques_seq_no = document.getElementById("curr_ques_seq_no").value;
  var total_no_ques = document.getElementById("total_no_ques").value;
   var quesSeqArray = document.getElementById("ques_sequence_array").value;
  $("#tempdiv").remove();
  if (total_no_ques > 1 ) {
    $.ajax({
      type: "get",
      url: "/user_preferences/get_next_interest_question/",
      dataType: "html",
      // data: {ques_sequence_array: quesSeqArray , curr_ques_seq_no: curr_ques_seq_no },
      data: { ques_sequence_array: quesSeqArray  },
      success: function(data){
          $('#ques_container').html(data);


            // $('#preference-image').hide('slow');

      },
      error: function (err){}
    });
  }
});

  $(".updateQuesAns").click(function(){
    var selectedImageId = [];
    var quesNo = [];
    $("input:checked").each(function() {
      selectedImageId.push($(this).val());
    });
    quesNo.push(document.getElementById("quesNo").value);
    storeData.push({question_id: quesNo , category_item_id: selectedImageId })
    // console.log(storeData);
    $("#tempdiv").remove();
    $.ajax({
      type: "get",
      url: "/user_preferences/update_user_interest" ,
      dataType: "json",
      data: {user_interest: storeData },
      success: function(data){
        // location.reload();
        window.location.href = ""+data.path_to ;
      },
      error: function (err){}
    });
  });

  $('#btn_next').click(function () {
    if ($('.type_radio_btn:checked').size() && $('.radio_budget:checked').size()) {
      $('#error_message').html("");
      $('.type_of_house').hide('slow');
      $('.spaces').hide('slow');
      $('.budget').hide('slow');
      $('#form_next').hide('slow');

      $('.floor_plan_image').show('slow');
      $('.upload_design').show('slow');
      $('.description').show('slow');
      $('.proceed_btn').show('slow');
    }else {
      $('#error_message').html("Please Select Type of House & Budget!!");
    }
  });

  $('.form_back_btn').click(function () {
    $('#error_message').html("");
    $('.type_of_house').show('slow');
    $('.spaces').show('slow');
    $('.budget').show('slow');
    $('#form_next').show('slow');

    $('.floor_plan_image').hide('slow');
    $('.upload_design').hide('slow');
    $('.description').hide('slow');
    $('.proceed_btn').hide('slow');
  });


};

// Select an Image - vendor
var chooseImage;
chooseImage = function(item_id, category_id, checked) {
url = '/users/preferences?category_id=' + category_id + '&category_item_id=' + item_id + '&checked=' + checked;
$.post(url, function(data) {
  if(data !== undefined && data.error) {
    alert('You cannot select more than 10 images');
    $('.select-image.select-image-'+ item_id).removeAttr( "checked" );
  }
  $(".select-image-" + item_id).attr("onclick", "chooseImage(" + item_id + ", " + category_id + ", " + !checked + ")")
  });
};

var backToCategory;
backToCategory = function(){
  // $('.category-items').hide('slow');
  $('#preference-image').show();
  // Slide to next unchecked category
  $('.slick-next').trigger('click');

  $('html,body').animate({
      scrollTop: $("#preference-image").offset().top},'slow');
};

$(document).on('ready page:update', function(event) {
  $(".finish-btn a").tooltip();
  $(".next-btn button").tooltip();

  $(".category-items img").tooltip();
});

$(document).ready(ready);
$(document).on('page:load', ready); // Loads javascript while loading page

function show_form() {
  $('#client_form').hide();
  $('#client_form').show('slow');
  // $('#reset_preference').hide();
}
function hide_form() {
  $('#client_form').hide('slow');
}
