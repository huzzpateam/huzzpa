var ready;
ready = function() {

  $( "#vendors" ).change(function() {
     var vendorStatus = $(this).val();
     if(vendorStatus === "all"){
        $('.filter-vendor').show('slow');
        return false;
     }
     $('.filter-vendor').hide();
     $('.filter-vendor').each(function() {
       div = $(this);
       if(div.data('vendor') === vendorStatus ) {
         div.show('slow');
       }
     });
  });
};// Loads javascript while loading page

function get_client_profile(user_id) {
  // var user_id = $('.user_name_nav li.active')[0].getAttribute('data-userid');
  var url = '/admins/get_user_profile?user_id=' + user_id ;
 $.get(url, function(data) {

   $('#view_user_profile').html(data); } ).done(function() {
    // reinit_slick();
    $('#show_profile').modal('show');
    //Image popup
    $('.img-pop').on('click',function(){
        var sr=$(this).attr('src');
       $('#mimg').attr('src',sr);
       $('#myPopupModal').modal('show');
    });
    reinit_slick();
  });

}


function reinit_slick() {
  $('.admins-user-profile-slider').slick({
  dots: false,
  infinite: true,
  speed: 1000,
  slidesToShow: 3,
  slidesToScroll: 1,
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1,
        infinite: true,
        dots: false
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
});
}

$(document).ready(ready);
$(document).on('page:load', ready);

var clearForm;
  clearForm = function(){
    document.getElementById("user_search").reset();
  };
