# ready = undefined
#
# ready = ->
#   totalChars = 100
#   #Total characters allowed in textarea
#   countTextBox = $('#vendor_profile_about')
#   # Textarea input box
#   charsCountEl = $('#countchars')
#   # Remaining chars count will be displayed here
#   charsCountEl.text totalChars
#   #initial value of countchars element
#   countTextBox.keyup ->
#     #user releases a key on the keyboard
#     thisChars = @value.replace(/{.*}/g, '').length
#     #get chars count in textarea
#     if thisChars > totalChars
#       CharsToDel = thisChars - totalChars
#       # total extra chars to delete
#       @value = @value.substring(0, @value.length - CharsToDel)
#       #remove excess chars from textarea
#     else
#       charsCountEl.text totalChars - thisChars
#       #count remaining chars
#     return
#   return
#
# $(document).ready ready
# $(document).on 'page:load', ready
# # Loads javascript while loading page
# For slider in vendor preference page
ready = ->
  # For slider in vendor preference page
  $('.vendor-public-preference-slider').slick
    dots: false
    infinite: true
    speed: 1000
    slidesToShow: 2
    slidesToScroll: 1
    responsive: [
      {
        breakpoint: 1024
        settings:
          slidesToShow: 2
          slidesToScroll: 1
          infinite: true
          dots: false
      }
      {
        breakpoint: 600
        settings:
          slidesToShow: 2
          slidesToScroll: 1
      }
      {
        breakpoint: 480
        settings:
          slidesToShow: 1
          slidesToScroll: 1
      }
    ]
  return

$(document).ready ready
$(document).on 'page:load', ready
# Loads javascript while loading page

jQuery ->
  $(document).on "upload:start", "form", (e) ->
    $(this).find("input[type=submit]").attr "disabled", true
    $("#vendorImage").text("Uploading...")

  $(document).on "upload:progress", "form", (e) ->
    $("#vendorImage").text("please wait while image uploads....")
    # detail          = e.originalEvent.detail
    # percentComplete = Math.round(detail.loaded / detail.total * 100)
    # $("#vendorImage").text("#{percentComplete}% uploaded")

  $(document).on "upload:success", "form", (e) ->
    $(this).find("input[type=submit]").removeAttr "disabled"  unless $(this).find("input.uploading").length
    $("#vendorImage").text("image looks good!! click save at the end")
