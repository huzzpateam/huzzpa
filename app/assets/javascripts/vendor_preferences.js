  var ready;
  ready = function() {
    $(".category-slider img").tooltip();

  // handle Category checkboxes
  $( ".select-category" ).change(function() {
        url = '/vendor_preferences/select?category_id=' + $(this).val();
        $.get(url, function(data) {
          $('.category-items').html(data);
        });

      var categoryVisible = $('#preference-image').is(':visible');
      if(categoryVisible === true) {
        $('#preference-image').hide('slow');
      }
  });

  // // For slider in vendor preference page
  // $('.preference-slider').slick({
  //   dots: false,
  //   infinite: true,
  //   speed: 1000,
  //   slidesToShow: 3,
  //   slidesToScroll: 1,
  //   responsive: [
  //     {
  //       breakpoint: 1024,
  //       settings: {
  //         slidesToShow: 2,
  //         slidesToScroll: 1,
  //         infinite: true,
  //         dots: false
  //       }
  //     },
  //     {
  //       breakpoint: 600,
  //       settings: {
  //         slidesToShow: 2,
  //         slidesToScroll: 1
  //       }
  //     },
  //     {
  //       breakpoint: 480,
  //       settings: {
  //         slidesToShow: 1,
  //         slidesToScroll: 1
  //       }
  //     }
  //   ]
  // });

};

var backToCategory;
backToCategory = function(){
  // $('.category-items').hide('slow');
  $('#preference-image').show();
  // Slide to next unchecked category
  $('.slick-next').trigger('click');

  $('html,body').animate({
      scrollTop: $("#preference-image").offset().top},'slow');
};



// Select an Image - vendor
var selectImage;
selectImage = function(item_id, category_id, checked) {
  url = '/vendor_preferences?category_id=' + category_id + '&category_item_id=' + item_id + '&checked=' + checked;
  $.post(url, function(data) {
    if(data !== undefined && data.error) {
      alert('You cannot select more than 10 images');
      $('.select-image.select-image-'+ item_id).removeAttr( "checked" );
    }
    $(".select-image-" + item_id).attr("onclick", "selectImage(" + item_id + ", " + category_id + ", " + !checked + ")")
  });
};

$(document).on('ready page:update', function(event) {

  $(".finish-btn a").tooltip();
  $(".next-btn button").tooltip();

  $(".category-items img").tooltip();

});

$(document).ready(ready);
$(document).on('page:load', ready); // Loads javascript while loading page
