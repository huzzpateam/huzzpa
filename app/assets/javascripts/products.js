

function goBack() {
  window.history.back();
}

function set_favourate_btn(product_id) {
    // Favourate icon is not liked
    $.ajax({
      type: "post",
      url: "/fav_products",
      dataType: "json",
      data: {product_id: product_id },
      success: function(data){
        // $('.fav'+product_id).removeClass("fa-heart-o").addClass("fa-heart");
        $('#prod_fav_btn').hide();
        $('#prod_rem_btn').show();
      },
      error: function (err){
      }
    });
}


function remove_favourate_btn(product_id) {

  $.ajax({
        type: "get",
        url: "/fav_products/remove_fav_prod",
        dataType: "json",
        data: {product_id: product_id},
        complete: function(){
            // Favourate icon is liked
            // $('.fav'+product_id).removeClass("fa-heart").addClass("fa-heart-o");
            $('#prod_rem_btn').hide();
            $('#prod_fav_btn').show();
        }
    });
}
