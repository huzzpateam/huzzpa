// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require jquery-ui
//= require chat
//= require private_pub
//= require ckeditor/init
//= require twitter/bootstrap
//= require bootstrap-select
//= require bootstrap-switch
//= require turbolinks
//= require refile
//= require_tree .
//= require_tree ./ckeditor
var ready;
ready = function() {

  $('.selectpicker').selectpicker('render');
//   setTimeout(function() {
//     $('#flash').hide("slow");
// }, 3000);
  $("#flash").delay(4000).slideUp(500, function(){
      $("#flash").alert('close');
  });
  // $("#flash").delay(3000).addClass("animated fadeIn");
  //  setTimeout(function() {
  //    $("#flash").css('display', 'none');
  //  }, 3000);
  chat_closer();
  $('#chat_notification_icon').hide();
};


function chat_closer() {
  $( "#chat_close_btn" ).click(function() {
    $("#floatdiv").hide();
  });

  $("#floatchat_btn").click(function() {
    $("#floatdiv").show();
    $('.chat_name_tab > li.active > a' ).click();
    $('#chat_notification_icon').hide();


  });
}

$(document).on('page:change', function(){
  $(".edit-image img").tooltip();
  $(".tab-buttons .pro").tooltip();
  $(".tab-buttons .pref").tooltip();
  $(".tab-buttons .wrk").tooltip();
  $(".tab-buttons .match").tooltip();
  $(".mytooltip").tooltip();

  $("#flash").delay(4000).slideUp(500, function(){
      $("#flash").alert('close');
  });
  // $("#flash").delay(3000).addClass("animated fadeIn");
  //  setTimeout(function() {
  //    $("#flash").css('display', 'none');
  //  }, 3000);

  chat_closer();

  (function (w,i,d,g,e,t,s) {w[d] = w[d]||[];t= i.createElement(g);
    t.async=1;t.src=e;s=i.getElementsByTagName(g)[0];s.parentNode.insertBefore(t, s);
  })(window, document, '_gscq','script','//widgets.getsitecontrol.com/111805/script.js');
  
});


$(document).ready(ready);
$(document).on('page:load', ready); // Loads javascript while loading page
