# Vendor preferences
class VendorPreference < ActiveRecord::Base
  belongs_to :vendor, class_name: 'User', foreign_key: 'vendor_id'
  belongs_to :category_item
  belongs_to :category
  validates_uniqueness_of :vendor_id, scope: [:category_item_id, :preference_no]
end
