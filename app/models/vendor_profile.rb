class VendorProfile < ActiveRecord::Base
  belongs_to :vendor, class_name: 'User', foreign_key: 'vendor_id'
  attachment :profile_image
  has_many :chat_accesses
end
