# Category item model
class CategoryItem < ActiveRecord::Base
  belongs_to :category
  attachment :image
  belongs_to :type
  has_many :questioners
  has_many :user_interests
end
