class Product < ActiveRecord::Base
  belongs_to :category
  attachment :product_image
  scope :any_tags, -> (tags){where('tags && ARRAY[?]', tags)}
  scope :all_tags, -> (tags){where('tags @> ARRAY[?]', tags)}
end
