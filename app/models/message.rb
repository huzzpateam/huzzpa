class Message < ActiveRecord::Base
  belongs_to :conversation
  belongs_to :user

  has_many :images, dependent: :destroy
  #  attachment :image
  accepts_attachments_for :images, attachment: :file, append: true

   validates_presence_of :body, :conversation_id, :user_id
end
