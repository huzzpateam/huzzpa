# User model
class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable, :confirmable,
         :recoverable, :rememberable, :trackable, :validatable, :timeoutable,
         :omniauthable, :omniauth_providers => [:facebook, :google_oauth2]
  ROLES = %i(vendor user)
  validates :role, presence: true
  validates :name, presence: true
  validates :phone, presence: true
  has_one :user_profile, foreign_key: 'user_id', class_name: 'UserProfile'#, dependent: :destroy
  has_many :user_preferences, foreign_key: 'user_id', class_name: 'UserPreference'#, dependent: :destroy
  has_one :vendor_profile, foreign_key: 'vendor_id', class_name: 'VendorProfile' #, dependent: :destroy
  has_many :vendor_works, foreign_key: 'vendor_id', class_name: 'VendorWork' #, dependent: :destroy
  has_many :vendor_preferences, foreign_key: 'vendor_id', class_name: 'VendorPreference' #, dependent: :destroy

  #For chat
  has_many :conversations, :foreign_key => :sender_id

  has_many :chat_accesses
  has_many :fav_products

  has_many :comments , dependent: :destroy

  has_one :user_detail, dependent: :destroy
  has_many :user_interests, dependent: :destroy

  scope :vendor, -> { where(role: 'vendor') }
  scope :user, -> { where(role: 'user') }
  # has_many :vendor_portfolio_images, through: :vendor_works, foreign_key: 'vendor_id', class_name: 'User', dependent: :destroy

  def self.from_omniauth(auth, role)
    data = auth['info']
    email = data['email']
    name = data['name']
    role = role ? role : "user"
    phone = '+91-XXX'
    provider = auth['provider']
    uid = auth['uid']
    user = User.where(email: email).first
    if user
      user.provider = auth.provider
      user.uid = auth.uid
    else
      user = User.new(
        name: name, email: email, phone: phone, role: role, uid: uid,
        password: Devise.friendly_token[0, 20], provider: provider
      )
      user.skip_confirmation!
      user.save!
      AdminMailer.delay.new_user(user)
    end
    user
  end

  def self.new_with_session(params, session)
    super.tap do |user|
      if data = session["devise.user_data"]
        user.email = data['email'] if user.email.blank?
        user.name = data['info']['name']
        user.role = session["devise.role"]
        user.valid?
      end
    end
  end

  def password_required?
    super && provider.blank?
  end

  def after_confirmation
    welcome_email
  end

  # before_create :create_profile

  # # Override Devise Method to delay email notification using Active Record Delayed Job
  # def send_devise_notification(notification, *args)
  #   devise_mailer.send(notification, self, *args).deliver_later
  # end

  def admin?
    role == 'admin'
  end

  def vendor?
    role == 'vendor'
  end

  def user?
    role == 'user'
  end
  # Time for a users session to be destroyed.
  def timeout_in
    if self.admin?
      1.year
    else
      10.days
    end
  end

  def self.to_csv
    attributes = %w{id email name}

    CSV.generate(headers: true) do |csv|
      csv << attributes

      all.each do |user|
        csv << attributes.map{ |attr| user.send(attr) }
      end
    end
  end

  protected
    def welcome_email
      WelcomeMailer.welcome_new_user(self).deliver_now
    end
    # def create_profile
    #   if self.role == 'vendor'
    #     self.build_vendor_profile(company: 'I work for?', about: 'Something interesting about me', address: 'I stay at?',
    #                                       website: 'whats your online address?')
    #   # Will change this code once I start wokring in User module.
    #   elsif self.role == 'user'
    #     self.build_user_profile(location: 'Location of the work', pincode: 0000, work_details: 'Something about the work')
    #   end
    # end
end

# Code for implementing logics for Delayed Job
# after_commit :send_pending_notifications
#
#             protected
#
#             def send_devise_notification(notification, *args)
#               # If the record is new or changed then delay the
#               # delivery until the after_commit callback otherwise
#               # send now because after_commit will not be called.
#               if new_record? || changed?
#                 pending_notifications << [notification, args]
#               else
#                 devise_mailer.send(notification, self, *args).deliver
#               end
#             end
#
#             def send_pending_notifications
#               pending_notifications.each do |notification, args|
#                 devise_mailer.send(notification, self, *args).deliver
#               end
#
#               # Empty the pending notifications array because the
#               # after_commit hook can be called multiple times which
#               # could cause multiple emails to be sent.
#               pending_notifications.clear
#             end
#
#             def pending_notifications
#               @pending_notifications ||= []
#             end
# has_many :vendor_portfolio_images, through: :vendor_works, foreign_key: 'vendor_id', class_name: 'User', dependent: :destroy

# def after_confirmation
#   # call back from devise right after user clicks on confirmation link
# end

# before_create :create_profile

# # Override Devise Method to delay email notification using Active Record Delayed Job
# def send_devise_notification(notification, *args)
#   devise_mailer.send(notification, self, *args).deliver_later
# end
