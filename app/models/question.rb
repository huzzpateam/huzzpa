class Question < ActiveRecord::Base
	has_many :questioners
	has_many :user_interests
	has_many :question_sequences
end
