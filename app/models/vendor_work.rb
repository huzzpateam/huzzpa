class VendorWork < ActiveRecord::Base
  belongs_to :vendor, class_name: 'User', foreign_key: 'vendor_id'
  has_many :vendor_portfolio_images, dependent: :destroy
  accepts_attachments_for :vendor_portfolio_images, attachment: :file, append: true
end
