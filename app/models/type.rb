class Type < ActiveRecord::Base
  belongs_to :category
  has_many :category_items
  has_many :blogs
  scope :category_id, -> (category_id) { where(category_id: category_id) }

end
