class Category < ActiveRecord::Base
  has_many :category_items
  has_many :tags
  has_many :questioners
  attachment :profile_image
  has_many :blogs
end
