class HuzzpaWork < ActiveRecord::Base
  # attachment :image
  attachment :logo

  has_many :project_images, dependent: :destroy
  accepts_attachments_for :project_images, attachment: :file, append: true
end
