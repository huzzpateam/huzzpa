class ProjectImage < ActiveRecord::Base
  belongs_to :huzzpa_work
  attachment :file
end
