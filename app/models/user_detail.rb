class UserDetail < ActiveRecord::Base
  belongs_to :user
  attachment :floor_plan_image
  # attachment :flat_image

  has_many :flat_images, dependent: :destroy
  accepts_attachments_for :flat_images, attachment: :file, append: true
end
