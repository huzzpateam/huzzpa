class UserProfile < ActiveRecord::Base
  belongs_to :user
  attachment :profile_image
end
