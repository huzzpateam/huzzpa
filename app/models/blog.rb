class Blog < ActiveRecord::Base
  extend FriendlyId
  friendly_id :title, use: :slugged

    belongs_to :category
    attachment :image
    has_many :comments , dependent: :destroy
end
