class FlatImage < ActiveRecord::Base
  belongs_to :user_detail
  attachment :file
end
