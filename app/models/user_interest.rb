class UserInterest < ActiveRecord::Base
  belongs_to :user
  belongs_to :question
  belongs_to :category_item
end
