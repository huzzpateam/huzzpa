class Questioner < ActiveRecord::Base
  belongs_to :question
  belongs_to :category_item
  belongs_to :category
end
