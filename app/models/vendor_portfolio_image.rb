class VendorPortfolioImage < ActiveRecord::Base
  belongs_to :vendor_work
  attachment :file
end
