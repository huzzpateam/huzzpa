class HuzzpaWorksController < ApplicationController
  before_action :set_huzzpa_work, only: [:show, :edit, :update, :destroy]
  before_filter :authenticate_user!
  before_filter :authorize_admin

  # GET /huzzpa_works
  # GET /huzzpa_works.json
  def index
    @huzzpa_works = HuzzpaWork.all.paginate(page: params[:page], per_page: 5)
  end

  # GET /huzzpa_works/1
  # GET /huzzpa_works/1.json
  def show
  end

  # GET /huzzpa_works/new
  def new
    @huzzpa_work = HuzzpaWork.new
  end

  # GET /huzzpa_works/1/edit
  def edit
  end

  # POST /huzzpa_works
  # POST /huzzpa_works.json
  def create
    @huzzpa_work = HuzzpaWork.new(huzzpa_work_params)

    respond_to do |format|
      if @huzzpa_work.save
        format.html { redirect_to huzzpa_works_path, notice: 'Huzzpa work was successfully created.' }
        format.json { render :show, status: :created, location: @huzzpa_work }
      else
        format.html { render :new }
        format.json { render json: @huzzpa_work.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /huzzpa_works/1
  # PATCH/PUT /huzzpa_works/1.json
  def update
    respond_to do |format|
      if @huzzpa_work.update(huzzpa_work_params)
        format.html { redirect_to huzzpa_works_path, notice: 'Huzzpa work was successfully updated.' }
        format.json { render :show, status: :ok, location: @huzzpa_work }
      else
        format.html { render :edit }
        format.json { render json: @huzzpa_work.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /huzzpa_works/1
  # DELETE /huzzpa_works/1.json
  def destroy
    @huzzpa_work.destroy
    respond_to do |format|
      format.html { redirect_to huzzpa_works_url, notice: 'Huzzpa work was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_huzzpa_work
      @huzzpa_work = HuzzpaWork.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def huzzpa_work_params
      params.require(:huzzpa_work).permit(:logo , :name, :location , :description , project_images_files: [])
    end
end
