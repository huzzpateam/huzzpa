# Overriding the devise sessions controller,
class SessionsController < Devise::SessionsController
  def after_sign_in_path_for(resource)
    if resource.role == 'vendor'
      vendor_profile_path(resource.vendor_profile)
    elsif resource.role == 'user'
      # user_profile_path(resource.user_profile)
      if resource.user_profile.preference_no.present?
        matched_vendors_path
      else
        preferences_path
      end

    else
      categories_path
    end
  end
end
