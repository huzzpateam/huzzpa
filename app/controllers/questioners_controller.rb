class QuestionersController < ApplicationController
  before_action :set_questioner, only: [:show, :edit, :update, :destroy]
  before_filter :authenticate_user!
  before_filter :authorize_admin
  layout false, only: :get_category_item

  # GET /questioners
  # GET /questioners.json
  def index
    @ques_cat_ids = Questioner.pluck(:question_id , :category_id).uniq
    @questioners = Questioner.all
  end

  # GET /questioners/1
  # GET /questioners/1.json
  def show
  end

  # GET /questioners/new
  def new
    @questioner = Questioner.new
  end

  # GET /questioners/1/edit
  def edit
  end

  def get_category_item

    @category_items = CategoryItem.where(category_id: params[:category_id] )


  end


  # POST /questioners
  # POST /questioners.json

  def create
    Questioner.where(question_id: questioner_params[:question_id]).delete_all
    if  questioner_params[:category_item_id].present?
      questioner_params[:category_item_id].each do |key|
        @questioner = Questioner.new("question_id" => questioner_params[:question_id] , "category_id" => questioner_params[:category_id] , "category_item_id" => key)
        @questioner.save
      end
      respond_to do |format|
          format.html { redirect_to questioners_path, notice: 'Questioner was successfully created.' }
          format.json { render :show, status: :created, location: @questioner }
      end
    else
      respond_to do |format|
        format.html { redirect_to :back , notice: 'Questioner was not created.' }
        format.json { render json: @questioner.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /questioners/1
  # PATCH/PUT /questioners/1.json
  def update
    respond_to do |format|
      if @questioner.update(questioner_params)
        format.html { redirect_to @questioner, notice: 'Questioner was successfully updated.' }
        format.json { render :show, status: :ok, location: @questioner }
      else
        format.html { render :edit }
        format.json { render json: @questioner.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /questioners/1
  # DELETE /questioners/1.json
  def destroy
    if  QuestionSequence.where(question_id: params[:id]).present?
      respond_to do |format|
        format.html { redirect_to questioners_path, notice: 'Question is in Active State . Please Remove from Active Question!' }
        format.json { head :no_content }
      end
    else
      @questioner.destroy_all
      respond_to do |format|
        format.html { redirect_to questioners_path, notice: 'Questioner was successfully destroyed.' }
        format.json { head :no_content }
      end
    end

  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_questioner
      @questioner = Questioner.where(question_id: params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def questioner_params
       # params.require(:questioner).permit(:question_id)
       params.permit(:question_id , :category_id ,:category_item_id => [])

    end
end
