class BlogsController < ApplicationController
  before_action :set_blog, only: [:show, :edit, :update, :destroy]
  before_filter :authenticate_user! , except: [:index , :show , :template , :get_blogby_category , :get_latest_blogs , :get_blogby_type]
  before_filter :authorize_admin , only: [:admin_blog , :new]
  layout false, only: [:get_blogby_category ,:get_latest_blogs, :get_blogby_type , :template]

  # GET /blogs
  # GET /blogs.json
  def index


    @categories =  Blog.uniq.pluck(:category_id)

    if  params[:blog_category].present?
      @blogs = Blog.where(category_id: params[:blog_category]).order("created_at desc").paginate(page: params[:page], per_page: 20)
      @category_id = params[:blog_category].to_i
    elsif  params[:blog_type].present?
      @blogs = Blog.where(type_id: params[:blog_type]).order("created_at desc").paginate(page: params[:page], per_page: 20)
      @type_id = params[:blog_type].to_i
      @category_id = Type.find(@type_id).category_id
    else
      @blogs = Blog.all.order("created_at desc").paginate(page: params[:page], per_page: 20)
    end

  end

  # GET /blogs/1
  # GET /blogs/1.json
  def show

    @comments = Comment.where(blog: @blog)
    @comments = @comments.hash_tree
    @comment = Comment.new(parent_id: params[:parent_id])
    if user_signed_in?
      if current_user.role != "admin"
        @blog.visitor_count = @blog.visitor_count.next
        @blog.save
      end
    else
      @blog.visitor_count = @blog.visitor_count.next
      @blog.save
    end


  end

  def template
  end

  def get_blogby_category
    @blogs = Blog.where(get_blog_category_params).order("created_at desc")
    respond_to do |format|
      format.html
    end
  end

  def get_latest_blogs
    @blogs = Blog.all.order("created_at desc").paginate(page: params[:page], per_page: 20)
    respond_to do |format|
      format.html
    end
  end

  def get_blogby_type
    @blogs = Blog.where(get_blog_type_params).order("created_at desc")
    respond_to do |format|
      format.html
    end
  end

  def admin_blog
    @blogs = Blog.all.order("created_at desc").paginate(page: params[:page], per_page: 20)
  end

  # GET /blogs/new
  def new
    @blog = Blog.new
  end

  # GET /blogs/1/edit
  def edit
  end

  # POST /blogs
  # POST /blogs.json
  def create
    @blog = Blog.new(blog_params)

    respond_to do |format|
      if @blog.save
        format.html { redirect_to admin_blog_blogs_url, notice: 'Blog was successfully created.' }
        format.json { render :show, status: :created, location: @blog }
      else
        format.html { render :new }
        format.json { render json: @blog.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /blogs/1
  # PATCH/PUT /blogs/1.json
  def update
    respond_to do |format|
      if @blog.update(blog_params)
        format.html { redirect_to admin_blog_blogs_url, notice: 'Blog was successfully updated.' }
        format.json { render :show, status: :ok, location: @blog }
      else
        format.html { render :edit }
        format.json { render json: @blog.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /blogs/1
  # DELETE /blogs/1.json
  def destroy
    @blog.destroy
    respond_to do |format|
      format.html { redirect_to admin_blog_blogs_url, notice: 'Blog was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_blog
      @blog = Blog.friendly.find(params[:id])

      rescue ActiveRecord::RecordNotFound => e
        redirect_to blogs_path
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def blog_params
        params.require(:blog).permit(:title, :subtitle, :image, :author, :body , :category_id , :type_id)
    end
    def get_blog_category_params
       params.permit(:category_id)
    end

    def get_blog_type_params
       params.permit(:type_id)
    end

end
