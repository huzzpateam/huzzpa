class MatchedUsersController < ApplicationController
  before_action :authenticate_user!
  before_action :authorize_vendor
  before_action :authorize_vendor_activation

  def index
    vendor_preference_no = current_user.vendor_profile.preference_no
    if !vendor_preference_no.nil?
      @vendor_selected_item = VendorPreference.where(vendor: current_user, preference_no: vendor_preference_no).pluck(:category_item_id)
      users_preference_no = UserProfile.pluck(:preference_no).reject(&:nil?)
      @users_preferences = UserPreference.where(preference_no: users_preference_no).to_a
      @results = []
      @users_id = @users_preferences.map(&:user_id).uniq
      @users_id.each do |v|
        prefs = (@users_preferences.select { |vp| vp.user_id == v }).map(&:category_item_id)
        @results << { user_id: v, prefs: prefs }
      end
      @results.each do |r|
        r[:matched] = r[:prefs] & @vendor_selected_item
        r[:matched_perct] = (r[:matched].size / @vendor_selected_item.count) * 100
      end
      @results = @results.sort_by {|r| r[:matched_perct]}.reverse
      # @res = @results[0..5]
      @users = User.includes(:user_profile).where(id: @results.map { |r| r[:user_id] }, guest: false).limit(6)
    else
      flash[:notice] = 'Oops! You need to choose your preferences before we suggest someone.'
      redirect_to vendor_preferences_path
    end
  end

  def connect
    @user_id = params[:user_id]
    user = User.find_by(id: @user_id)
    AdminMailer.delay.connect_user(current_user, user)
    # UserMailer.connect(current_user, user).deliver_now
    VendorMailer.delay.ack(current_user)
    flash[:notice] = "Thank you #{current_user.name}, we will get back to you within 24 hours."
    redirect_to :back
  end
end
