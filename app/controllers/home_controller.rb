# Home controller
class HomeController < ApplicationController
  # Home page
  def index
  end

  def about
  end

  def terms
  end

  def privacy
  end

  def profile
    @projects = HuzzpaWork.all
  end

  def user_role
    session["devise.role"] = 'user'
    redirect_to new_user_session_path if params[:path] == 'login'
    redirect_to new_user_registration_path
  end

  def vendor_role
    session["devise.role"] = 'vendor'
    redirect_to new_user_session_path if params[:path] == 'login'
    redirect_to new_user_registration_path
  end

  def contact_us
    if verify_recaptcha
      @name = params[:name]
      @email = params[:email]
      @message = params[:message]
      @mobile = params[:mobile]
      ContactMailer.delay.user_message(@name, @email, @mobile, @message)
      ContactMailer.delay.ack_message(@name, @email)
      flash[:notice] = "Thank you #{@name}!!, We will get back to you shortly."
      redirect_to root_path
    else
      redirect_to root_path
    end
    # if verify_recaptcha

    # else
    #   redirect_to root_path
    # end
  end

  def subscribe
    if params[:email]
      AdminMailer.delay.subscribe(params[:email])
      AdminMailer.delay.ack_subscribe(params[:email])
      redirect_to root_path, notice: 'Thank you for Signing up to Huzzpa newsletter, we will keep you posted!!'
    else
      redirect_to root_path, notice: 'Please provide your email-id!!'
    end
  end

  def faq
  end
end
