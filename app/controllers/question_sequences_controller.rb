class QuestionSequencesController < ApplicationController
  before_action :set_question_sequence, only: [:show, :edit, :update, :destroy]
  before_filter :authenticate_user!
  before_filter :authorize_admin

  # GET /question_sequences
  # GET /question_sequences.json
  def index
    @question_sequences = QuestionSequence.order(:question_id)
    @questioner = Questioner.all.pluck(:question_id).uniq
    @questions = Question.where(id: @questioner)

    @que_no =  QuestionSequence.pluck(:question_id)
    @no_of_ques_seq = QuestionSequence.count
  end

  # GET /question_sequences/1
  # GET /question_sequences/1.json
  def show
  end

  # GET /question_sequences/new
  def new
    # @question_sequence = QuestionSequence.new
  end

  # GET /question_sequences/1/edit
  def edit
  end

  def activate_questions
     get_question_id_params[:question_id].each do |key|
      @q = QuestionSequence.new("question_id" => key)
      @q.save
    end
     redirect_to :action => 'index'
  end

  def deactivate_questions

    get_question_id_params[:question_id].each do |key|
      @q = QuestionSequence.delete_all("question_id" => key)
    end
    redirect_to :back
  end

  def update_sequence

    @ques_and_seq = get_ques_and_seq_params;
    if QuestionSequence.exists?(:question_id => @ques_and_seq[:question_id])
      QuestionSequence.where(:question_id => @ques_and_seq[:question_id]).update_all(sequence_no: @ques_and_seq[:sequence_no])
    end
    redirect_to :action => 'index'
  end

  # POST /question_sequences
  # POST /question_sequences.json
  def create
    @question_sequence = QuestionSequence.new(question_sequence_params)

    respond_to do |format|
      if @question_sequence.save
        format.html { redirect_to @question_sequence, notice: 'Question sequence was successfully created.' }
        format.json { render :show, status: :created, location: @question_sequence }
      else
        format.html { render :new }
        format.json { render json: @question_sequence.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /question_sequences/1
  # PATCH/PUT /question_sequences/1.json
  def update
    respond_to do |format|
      if @question_sequence.update(question_sequence_params)
        format.html { redirect_to @question_sequence, notice: 'Question sequence was successfully updated.' }
        format.json { render :show, status: :ok, location: @question_sequence }
      else
        format.html { render :edit }
        format.json { render json: @question_sequence.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /question_sequences/1
  # DELETE /question_sequences/1.json
  def destroy
    @question_sequence.destroy
    respond_to do |format|
      format.html { redirect_to question_sequences_url, notice: 'Question sequence was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_question_sequence
      @question_sequence = QuestionSequence.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def question_sequence_params
      params[:question_sequence]
    end

    def get_question_id_params
      params.permit(:question_id  => [])

    end

    def get_ques_and_seq_params
      params.permit(:question_id , :sequence_no)
    end
end
