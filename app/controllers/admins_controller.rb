class AdminsController < ApplicationController
  before_filter :authenticate_user!
  before_filter :authorize_admin

  layout false, only: [ :get_user_profile ]

  def index
  end

  def vendors
    @search = User.where(role: 'vendor').includes(:vendor_profile).ransack(params[:q])
    @vendors = @search.result.order("created_at DESC").paginate(page: params[:page], per_page: 10)
  end

  def users
    @search = User.ransack(params[:q])
    @users = @search.result.where(role: 'user', guest: false).order("created_at DESC").paginate(page: params[:page], per_page: 10)
  end



  def guests
    @search = User.ransack(params[:q])
    @users = @search.result.where(role: 'user', guest: true).order("created_at DESC").paginate(page: params[:page], per_page: 10)
  end

  def status
    if params[:id] && params[:activate]
      vendor_id = params[:id]
      activate = params[:activate]
      @vendor = User.find(vendor_id)
      @vendor.update_attribute :activate, activate
      AdminMailer.delay.vendor_status_update(@vendor)
      redirect_to vendors_admins_path, notice: "Account status for #{@vendor.name} is updated!!"
    else
      redirect_to vendors_admins_path, notice: "Something went wrong, try again!!"
    end
  end

  def unblock
    if params[:id]
      vendor = VendorProfile.find_by(vendor_id: params[:id])
      vendor.update_attribute :selection_no, 0
      # AdminMailer.vendor_status_update(@vendor)
      redirect_to vendors_admins_path, notice: "You have unblocked this vendor"
    else
      redirect_to vendors_admins_path, notice: "Something went wrong, try again!!"
    end
  end

  def new_vendor
    @user = User.new
  end

  def create_vendor
    @user = User.new(vendor_params)
    @user.skip_confirmation!
    if @user.save
      user = VendorProfile.where(vendor: @user).first
      @user = VendorProfile.create(vendor: @user, company: 'I work for?', about: 'Something interesting about me', address: 'I stay at?',
                                        website: 'whats your online address?') unless user
      redirect_to vendors_admins_path, notice: "New Vendor has been created!!"
    else
      render :new_vendor
    end
  end

  def get_user_profile

    @user_interest = UserInterest.where(user_id: get_user_id)
    @ques = @user_interest.uniq.pluck(:question_id)
    @questions =  Question.find(@ques)
    @user_detail = UserDetail.where(user_id: get_user_id)
    @fav_products = FavProduct.where(user_id: get_user_id)

    respond_to do |format|
     format.html
    end
  end


  def export_users
    @users = User.where(role: 'user').where.not(["email ILIKE ?", "%guest_%"])
    respond_to do |format|
      format.csv { send_data @users.to_csv, filename: "users-#{Date.today}.csv" }
    end
  end

  def export_vendors
    @vendors = User.where(role: 'vendor')
    respond_to do |format|
      format.csv { send_data @vendors.to_csv, filename: "vendors-#{Date.today}.csv" }
    end
  end

  private

  def vendor_params
    params.require(:user).permit(:name, :email, :phone, :password, :password_confirmation, :role)
  end

  def get_user_id
     params.require(:user_id)
  end

end
