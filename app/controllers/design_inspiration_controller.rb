class DesignInspirationController < ApplicationController
  def kitchen
    @category_items = CategoryItem.where(category_id: 1).order("RANDOM()")
  end

  def wardrobes
    @category_items = CategoryItem.where(category_id: 10).order("RANDOM()")
  end

  def shoe_cabinet
    @category_items = CategoryItem.where(category_id: 14).order("RANDOM()")
  end

  def study
    @category_items = CategoryItem.where(category_id: 13).order("RANDOM()")
  end

  def vanity_unit
    @category_items = CategoryItem.where(category_id: 7).order("RANDOM()")
  end

  def tv_unit
    @category_items = CategoryItem.where(category_id: 11).order("RANDOM()")
  end

  def crockery_unit
    @category_items = CategoryItem.where(category_id: 12).order("RANDOM()")
  end


end
