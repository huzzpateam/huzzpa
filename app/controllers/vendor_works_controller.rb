class VendorWorksController < ApplicationController
  before_action :set_vendor_work, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!, except: [:all]
  before_action :authorize_vendor, except: [:all]
  before_action :authorized_vendor, only: [:edit, :update, :destroy]
  before_action :authorize_vendor_activation, except: [:all]

  # GET /vendor_works
  # GET /vendor_works.json
  def index
    @vendor_works = VendorWork.where(vendor: current_user)
  end

  # GET /vendor_works/1
  # GET /vendor_works/1.json
  def show
  end

  # GET /vendor_works/new
  def new
    @vendor_work = VendorWork.new
  end

  # GET /vendor_works/1/edit
  def edit
  end

  # POST /vendor_works
  # POST /vendor_works.json
  def create
    @vendor_work = current_user.vendor_works.build(vendor_work_params)

    respond_to do |format|
      if @vendor_work.save
        format.html { redirect_to vendor_works_path, notice: 'Your work has been added successfully.' }
        format.json { render :show, status: :created, location: @vendor_work }
      else
        format.html { render :new }
        format.json { render json: @vendor_work.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /vendor_works/1
  # PATCH/PUT /vendor_works/1.json
  def update
    respond_to do |format|
      if @vendor_work.update(vendor_work_params)
        format.html { redirect_to vendor_works_path, notice: 'Vendor work was successfully updated.' }
        format.json { render :show, status: :ok, location: @vendor_work }
      else
        format.html { render :edit }
        format.json { render json: @vendor_work.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /vendor_works/1
  # DELETE /vendor_works/1.json
  def destroy
    @vendor_work.destroy
    respond_to do |format|
      format.html { redirect_to vendor_works_url, notice: 'Vendor work was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  # def portfolio_images
  #   vendor_work_id = params[:vendor_work_id]
  #   @portfolio_images = VendorPortfolioImage.where(vendor_work_id: vendor_work_id)
  # end

  def all
    @vendor_works = VendorWork.order("RANDOM()")
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_vendor_work
    @vendor_work = VendorWork.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def vendor_work_params
    params.require(:vendor_work).permit(:description, :price, :vendor_id, :remove_image, vendor_portfolio_images_files: [])
  end

  def authorized_vendor
    redirect_to root_path, notice: 'You are not authorize to make this change' unless current_user == @vendor_work.vendor
  end
end
