class ChatAccessesController < ApplicationController
  before_action :set_chat_access, only: [:show, :edit, :update, :destroy]
  before_filter :authenticate_user!
  before_filter :authorize_admin

  # GET /chat_accesses
  # GET /chat_accesses.json
  def index
    @chat_accesses = ChatAccess.order('id')
    @conv = Conversation.all
  end

  # GET /chat_accesses/1
  # GET /chat_accesses/1.json
  def show
  end

  # GET /chat_accesses/new
  def new
    @chat_access = ChatAccess.new
  end

  # GET /chat_accesses/1/edit
  def edit
  end

  # POST /chat_accesses
  # POST /chat_accesses.json
  def create
    @chat_access = ChatAccess.new(chat_access_params)

    respond_to do |format|
      if @chat_access.save
        format.html { redirect_to @chat_access, notice: 'Chat access was successfully created.' }
        format.json { render :show, status: :created, location: @chat_access }
      else
        format.html { render :new }
        format.json { render json: @chat_access.errors, status: :unprocessable_entity }
      end
    end
  end

  def update_status
    @update_status = update_status_params
    @update_s = ChatAccess.where(id: @update_status[:id]).first
    @update_s.update(status: @update_status[:status])

    if  params[:status] == "true"
        AdminMailer.delay.approve_chat(@update_s.user , @update_s.vendor_profile.vendor )
    end
    
    respond_to do |format|
      format.html { redirect_to chat_accesses_url, notice: 'Updated successfully .' }
      format.json { head :no_content }
    end
  end

  # PATCH/PUT /chat_accesses/1
  # PATCH/PUT /chat_accesses/1.json
  def update

    respond_to do |format|
      if @chat_access.update(chat_access_params)
        format.html { redirect_to @chat_access, notice: 'Chat access was successfully updated.' }
        format.json { render :show, status: :ok, location: @chat_access }
      else
        format.html { render :edit }
        format.json { render json: @chat_access.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /chat_accesses/1
  # DELETE /chat_accesses/1.json
  def destroy
    @chat_access.destroy
    respond_to do |format|
      format.html { redirect_to chat_accesses_url, notice: 'Chat access was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_chat_access
      @chat_access = ChatAccess.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def chat_access_params
      params[:chat_access]
    end

    def update_status_params
      params.permit(:id , :status)
    end
end
