# Omniauth Callbacks for all strategies
class OmniauthCallbacksController < Devise::OmniauthCallbacksController

  def all
    role = session["devise.role"]
    user = User.from_omniauth(request.env["omniauth.auth"], role)
    if user.persisted?
      create_profile(user)
      sign_in(user)
      redirect_to preferences_path, notice: "Signed in successfully!!"
      # set_flash_message(:notice, :success, :kind => "Facebook") if is_navigational_format
    else
      session["devise.user_data"] = request.env["omniauth.auth"]
      redirect_to new_user_registration_url
    end
  end
  alias_method :facebook, :all
  alias_method :google_oauth2, :all

  def failure
    redirect_to root_path
  end

  private

  def create_profile(resource)
    if resource.role == 'vendor'
      vendor = VendorProfile.where(vendor: resource).first
      @vendor_profile = VendorProfile.create(vendor: resource, company: 'I work for?', about: 'Something interesting about me', address: 'I stay at?',
                                        website: 'whats your online address?') unless vendor
    elsif resource.role == 'user'
      user = UserProfile.where(user: resource).first
      @user_profile = UserProfile.create(user: resource, location: 'Location of the work',
                                  pincode: 0000, work_details: 'Something about the work') unless user
    elsif resource.role == 'admin'
      redirect_to root_path
    else
      redirect_to new_user_registration_path, notice: "We are very sorry, something went wrong! try Signing up from here"
    end
  end
end

# Use this peice of code only for facebook,
# facebook allows user to remove their email-id from the info they provide, if they do
# we can redirect User back to facebook scope and ask for email again as email-id is
# essential for devise to create an account.
# elsif request.env["omniauth.auth"].info.email.blank?
#   redirect_to "/users/auth/facebook?auth_type=rerequest&scope=email"
# end
