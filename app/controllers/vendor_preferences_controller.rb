# Vendor Preference
class VendorPreferencesController < ApplicationController
  before_filter :authenticate_user!
  before_filter :authorize_vendor
  before_filter :authorize_vendor_activation
  layout false, only: :select
  def index
    @selected = VendorPreferencesRelated.finish(current_user) if params[:finish] == 'true'
      # a = VendorPreference.includes(:category_item, :category).where(vendor: current_user)
      # @category_items = CategoryItem.where(id: @selected.map(&:category_item_id))
    preference_no = current_user.vendor_profile.preference_no
    unless preference_no.nil?
      @preferences = VendorPreference.includes(:category_item, :category).where(vendor: current_user, preference_no: preference_no)
      @prefer_details = CategoryItem.includes(:category).where(id: @preferences.map(&:category_item_id)
                                                                          ).order('categories.name')
    end
    # redirect_to matched_users_path if params[:finish] == 'true'
    redirect_to vendor_preferences_path if params[:finish] == 'true'

    @categories = Category.all
  end

  def select
    category_id = params[:category_id]
    @category = Category.find_by(id: category_id)
    @selected = VendorPreference.where(
      vendor: current_user,
      category_id: category_id, preference_no: nil).pluck(:category_item_id)
    @category_items = CategoryItem.where(category_id: params[:category_id]).first(6)
  end

  def create
    redirect_to vendor_preferences_path unless request.xhr?
    status = VendorPreferencesRelated.create(params, current_user)
    if status
      head :no_content
    else
      render json: { error: 'Failed' }
    end
  end

  # def selected
  #   @selected = VendorPreferencesRelated.finish(current_user) if params[:finish] == 'true'
  #   # a = VendorPreference.includes(:category_item, :category).where(vendor: 1)
  #   # @category_items = CategoryItem.where(id: @selected.map(&:category_item_id))
  #   @preferences = VendorPreference.includes(:category_item, :category).where(vendor: 1)
  #   @prefer_details = CategoryItem.includes(:category).where(id: @preferences.map(&:category_item_id)
  #                                                                               ).order('categories.name')
  # end
end
