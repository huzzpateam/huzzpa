class ConversationsController < ApplicationController
 before_filter :authenticate_user!

  layout false

  def create
    if Conversation.between(params[:sender_id],params[:recipient_id]).present?
      @conversation = Conversation.between(params[:sender_id],params[:recipient_id]).first
    else
      @conversation = Conversation.create!(conversation_params)
    end

    render json: { conversation_id: @conversation.id }
  end

  def show

    @conversation = Conversation.find(params[:id])
    @reciever = interlocutor(@conversation)
    # byebug
    @messages = Message.where(conversation: @conversation).order('created_at DESC').paginate(page: params[:page], per_page: 10)
    # .order('created_at DESC').reverse
    @message = Message.new
    respond_to do |format|
      format.html
      format.js
    end
  end

  private
  def conversation_params
    params.permit(:sender_id, :recipient_id)
  end

  def interlocutor(conversation)
    current_user == conversation.recipient ? conversation.sender : conversation.recipient
  end
end
