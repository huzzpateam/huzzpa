# Users preference logic
class UserPreferencesController < ApplicationController
  before_filter :authenticate_user!
  before_filter :authorize_user
  layout false, only: :select
   layout false, only: :update_user_interest


  layout false, only: :get_question
  layout false, only: :get_next_interest_question


  def index

    @selected = UserPreferencesRelated.finish(current_user) if params[:finish] == 'true'
    preference_no = current_user.user_profile.preference_no
    unless preference_no.nil?
      @preferences = UserPreference.includes(:category_item, :category).where(user: current_user, preference_no: preference_no)
      @prefer_details = CategoryItem.includes(:category).where(id: @preferences.map(&:category_item_id)).order('categories.name')
    end
    redirect_to matched_vendors_path if params[:finish] == 'true'



    @categories = Category.all

    # @firstQues = QuestionSequence.order(:sequence_no).first
    # @ques_sequence_array = QuestionSequence.order(:sequence_no).pluck(:sequence_no)
    # @ques_seq_no = 0
    #
    # @ques_no = QuestionSequence.where(sequence_no: @ques_sequence_array[@ques_seq_no])[0][:question_id]
    # @fetch_questioner = Questioner.where(question_id:  @ques_no)
    # @fetch_question = Question.find(@ques_no)

    @userdetails = UserDetail.new
    @categories = Category.all
    @user_interest = UserInterest.where(user_id: current_user.id)
    @ques = @user_interest.uniq.pluck(:question_id)
    @questions =  Question.find(@ques)
    @user_detail = UserDetail.where(user_id: current_user.id)
    @userprofile = UserProfile.where(user_id: current_user.id).first


  end

  def get_client_requirement
    @userdetails = UserDetail.new
    @categories = Category.all
    render :layout => false
  end

  def get_interest_questions

    @cate_id = UserDetail.where(user_id: current_user.id)[0].spaces
    # convert string array to interested_question_id
    @cate_id = @cate_id.map(&:to_i)

    #
    # @cate_item_id =  CategoryItem.where(category_id: @cate_id).pluck(:id)
    # @question_id = Questioner.where(category_item_id: @cate_item_id).pluck(:question_id)
    # @interested_question_id =  QuestionSequence.where(question_id: @question_id).pluck(:question_id)
    # @fetch_next_questioner = Questioner.where(question_id: @interested_question_id.first)
    # @current_ques_seq_no = 0
    # if @fetch_next_questioner.present?
    #     @fetch_next_question = Question.find(@interested_question_id.first)
    # end

    @ques_ids =  QuestionSequence.pluck(:question_id)
    @ques_cat_arr = Questioner.where(question_id: @ques_ids ).order(question_id: 'desc').pluck(:question_id , :category_id).uniq

    @ques_cat_interested_arr = []
    @ques_cat_arr.each do |val|
      if  @cate_id.include?(val[1])
        @ques_cat_interested_arr.push(val)
      end
    end
    general_id =  Category.where(name: ['General','general']).first.id
    temp_ary = []

    temp_ques_cat_interested_arr =   @ques_cat_interested_arr.inject([]) { |a,element| a << element.dup }

    temp_ques_cat_interested_arr.each_with_index do |ques|
      if ques.second == general_id
        temp_ary << ques
        @ques_cat_interested_arr.delete(ques)
      end
    end


    @ques_cat_interested_arr = [*@ques_cat_interested_arr, *temp_ary]

    @total_no_ques  = @ques_cat_interested_arr.count

    @curr_qid_cat_id =  @ques_cat_interested_arr.pop
    if   @curr_qid_cat_id.present?
      @curr_ques = Question.find(@curr_qid_cat_id[0])
      @curr_ques_images = Questioner.where(question_id: @curr_qid_cat_id[0])
    end


  end

  def get_next_interest_question

    # @interested_question_id = eval(get_ques_arr_seq_no_params[:ques_sequence_array])
    # @current_ques_seq_no = eval(get_ques_arr_seq_no_params[:curr_ques_seq_no]).next
    # @fetch_next_questioner = Questioner.where(question_id: @interested_question_id[@current_ques_seq_no] )
    # if @fetch_next_questioner.present?
    #     @fetch_next_question = Question.find(@interested_question_id[@current_ques_seq_no])
    # end

    @ques_cat_interested_arr = eval(get_ques_arr_seq_no_params[:ques_sequence_array])
    # @curr_ques = Question.find(@ques_cat_interested_arr.first[0])
    # @curr_ques_images = Questioner.where(question_id: @ques_cat_interested_arr.first[0])
    @total_no_ques  = @ques_cat_interested_arr.count

    @curr_qid_cat_id = @ques_cat_interested_arr.pop

    if   @curr_qid_cat_id.present?
      @curr_ques = Question.find(@curr_qid_cat_id[0])
      @curr_ques_images = Questioner.where(question_id: @curr_qid_cat_id[0])
    end

  end

  def get_question
    @curr_seq_no = get_current_seq_no_params[:current_seq_no]
    @ques_sequence_array = get_current_seq_no_params[:ques_sequence_array]
    @next_seq_no = @curr_seq_no.to_i + 1

    @ques_no = QuestionSequence.where(sequence_no: @ques_sequence_array[ @next_seq_no])[0][:question_id]
    @fetch_next_questioner = Questioner.where(question_id: @ques_no)
    @fetch_next_question = Question.find(@ques_no)
  end

  def update_user_interest
    @user_interests = get_user_interest_params;
    @user_interests.each do |user_interest|
      @queNo = user_interest[1][:question_id][0]
      if user_interest[1][:category_item_id].present?
          user_interest[1][:category_item_id].each do |categ_item_id|
            if  !UserInterest.where(user_id: current_user.id, question_id: @queNo, category_item_id: categ_item_id).present?
              UserInterest.create(user_id: current_user.id, question_id: @queNo, category_item_id: categ_item_id)
            end
          end
      end
    end

    # flash[:notice] = 'Your Interest was successfully captured.'
    # flash.keep(:notice)
    # render js: "window.location = '/matched_vendors'"



      if current_user.guest
        path  = "/guests/#{current_user.id}/edit"

      else
        path = "/matched_vendors"
      end

      render json: { path_to: path  }


      # puts matched_vendors_path
      # if request.xhr?
      #   respond_to do |format|
      #     format.js {render :js => "window.location = '#{matched_vendors_path}'"}
      #     # format.html {redirect_to matched_vendors_path }
      #   end
      # end

    # respond_to do |format|
    #     format.js { flash.keep[:notice] = "Your interests are successfully saved."}
    #     format.html { redirect_to matched_vendors_url , notice: 'Your Interest was successfully captured.' }
    #     #  format.html { redirect_to :back, notice: 'User profile was successfully created.' }
    # end
  end

  def select
    category_id = params[:category_id]
    @category = Category.find_by(id: category_id)
    @selected = UserPreference.where(
      user: current_user,
      category_id: category_id, preference_no: nil).pluck(:category_item_id)
    @category_items = CategoryItem.where(category_id: params[:category_id]).first(6)
  end

  def create
    redirect_to preferences_path unless request.xhr?
    status = UserPreferencesRelated.create(params, current_user)
    if status
      head :no_content
    else
      render json: { error: 'Failed' }
    end
  end


  private

  def get_current_seq_no_params
    params.permit(:ques_sequence_array => [])
  end

  def get_user_interest_params
    params.require(:user_interest).permit!
  end

  def get_ques_arr_seq_no_params
     params.permit(:ques_sequence_array )
  end

end
