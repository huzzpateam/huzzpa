class MessagesController < ApplicationController
	before_filter :authenticate_user!

  def create
    @conversation = Conversation.find(params[:conversation_id])
    @message = @conversation.messages.build(message_params)
    if params[:message][:images_files].present? && params[:message][:images_files][0] != "[]"
			if !params[:message][:body].present?
		 			@message.body = "Image"
		 	end
     @message.image_present = true
    end

    @message.user_id = current_user.id
    @message.save!


    @path = conversation_path(@conversation)

		# recip = current_user == @conversation.recipient ? @conversation.sender : @conversation.recipient
    # PrivatePub.publish_to("/notifications" + recip.id.to_s, cid: @conversation.id, sid: current_user.id, recip:  recip.id)
		# PrivatePub.publish_to("/messages/#{@conversation.id}", "$('#chat_notification_icon').show()")

		respond_to do |format|
      format.js #calls create.js.erb
    end

  end

  private

  def message_params
    params.require(:message).permit(:body , :image_type ,  images_files: [])
  end
end
