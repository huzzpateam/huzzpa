class MatchedVendorsController < ApplicationController
  before_action :authenticate_user!
  before_action :authorize_user
  before_action :authorize_only_user
  layout false, only: :get_product_filter_by_categ

  def index

    # user_preference_no = current_user.user_profile.preference_no
    # # if !user_preference_no.nil?
    #   @user_selected_item = UserPreference.where(user: current_user, preference_no: user_preference_no).pluck(:category_item_id)
    #   vendors_preference_no = VendorProfile.pluck(:preference_no).reject(&:nil?)
    #   @vendors_preferences = VendorPreference.where(preference_no: vendors_preference_no).to_a
    #   @results = []
    #   @vendors_id = @vendors_preferences.map(&:vendor_id).uniq
    #   @vendors_id.each do |v|
    #     prefs = (@vendors_preferences.select { |vp| vp.vendor_id == v }).map(&:category_item_id)
    #     @results << { vendor_id: v, prefs: prefs }
    #   end
    #   @results.each do |r|
    #     r[:matched] = r[:prefs] & @user_selected_item
    #     r[:matched_perct] = (r[:matched].size / @user_selected_item.count) * 100
    #   end
    #   @results = @results.sort_by {|r| r[:matched_perct]}.reverse
    #   @res = @results[0..5]
    #   @vendors = User.includes(:vendor_profile).where(id: @res.map { |r| r[:vendor_id] })
    # else
    #   flash[:notice] = 'Oops! You need to choose your preferences before we suggest someone.'
    #   redirect_to preferences_path
    # end
    # @vendors = VendorProfile.all
    # @user_vendors =  User.joins(:vendor_profile)

    @search = User.where(role: 'vendor' , activate: 'true').includes(:vendor_profile).ransack(params[:q])
    @vendors = @search.result.order("created_at DESC").paginate(page: params[:page], per_page: 10)
    @match_vendor_user_id = VendorProfile.pluck(:id , :vendor_id)
    @chat_vendor_id = ChatAccess.where(user: current_user.id).pluck(:vendor_profile_id)
    @allowed_chat_vendor_id = ChatAccess.where(user: current_user.id , status: true).pluck(:vendor_profile_id)

    # For Redomendation Engine Tag filter
    @categ_ids = UserInterest.where(user_id: current_user.id).pluck(:category_item_id)
    @categ_items = CategoryItem.where(id: @categ_ids)
    @tag_ids = []

    @categ_items.each do |categ_item|
      @tag_ids.push(*categ_item.tags)
    end
    @tag_ids = @tag_ids.uniq

    if @tag_ids.present?
       @products = Product.any_tags(@tag_ids)
    else
      @products = Product.all
    end
    @fav_prod_id = FavProduct.where(user_id: current_user.id).pluck(:product_id)

    @categ_id = @products.all.pluck(:category_id).uniq
    @categories = Category.where(id: @categ_id)
    @max_price = @products.pluck(:price).max



  end

  def get_product_filter_by_categ

    @products = Product.where(product_id_categ_id_params)

    @fav_prod_id = []
    if FavProduct.where(user_id: current_user.id).present?
      @fav_prod_id = FavProduct.where(user_id: current_user.id).pluck(:product_id)
    end

    @categ_id = @products.all.pluck(:category_id).uniq
    @categories = Category.where(id: @categ_id)


    respond_to do |format|
      format.html
    end

  end

  def chat_request

      @vendorid = VendorProfile.where(vendor_id: vendor_id_params)

      ChatAccess.create :user_id => current_user.id , vendor_profile_id:  @vendorid[0].id;

      flash.keep[:notice] = "Thank you #{current_user.name}, We send your request for approval !!
                      Allow us 24 hours to process your request."

  end

  def connect
    vendor_id = params[:vendor_id]
    vendor = User.find(vendor_id)

    @vendorid = VendorProfile.where(vendor_id: vendor_id)

    if !ChatAccess.where( user_id:  current_user.id , vendor_profile_id: @vendorid[0].id).present?
      ChatAccess.create :user_id => current_user.id , vendor_profile_id:  @vendorid[0].id
    end

    flash[:notice] = "Thank you #{current_user.name}, We are super excited about your project !!
                      Allow us 24 hours to connect with you."
    AdminMailer.delay.connect_vendor(current_user, vendor)
    VendorMailer.delay.connect(current_user, vendor)
    # UserMailer.delay.connect(current_user)
  redirect_to :back
  end
end

# For preference no: (current_user.user_profile.preference_no)
private

def vendor_id_params
    params.require(:vendor_id)
end

def product_id_categ_id_params
  params.permit(id: [] , category_id: [])

end
