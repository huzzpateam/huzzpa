class FavProductsController < ApplicationController
  before_action :set_fav_product, only: [:show, :edit, :update, :destroy]

  # GET /fav_products
  # GET /fav_products.json
  def index
    @fav_products = FavProduct.where(user_id: current_user.id)
  end

  # GET /fav_products/1
  # GET /fav_products/1.json
  def show
  end

  # GET /fav_products/new
  def new
    @fav_product = FavProduct.new
  end

  # GET /fav_products/1/edit
  def edit
  end

  # POST /fav_products
  # POST /fav_products.json
  def create



    @fav_product = FavProduct.new(product_id: fav_product_params , user_id: current_user.id)

    if  FavProduct.where(product_id: fav_product_params , user_id: current_user.id ).present?
      respond_to do |format|
        format.html { redirect_to @fav_product, notice: 'Fav product was already created.' }
        format.json { render :show, status: :created, location: @fav_product }
      end
    else
      respond_to do |format|
        if @fav_product.save
          format.html { redirect_to @fav_product, notice: 'Fav product was successfully created.' }
          format.json { render :show, status: :created, location: @fav_product }
        else
          format.html { render :new }
          format.json { render json: @fav_product.errors, status: :unprocessable_entity }
        end
      end
    end

  end

  # PATCH/PUT /fav_products/1
  # PATCH/PUT /fav_products/1.json
  def update
    respond_to do |format|
      if @fav_product.update(fav_product_params)
        format.html { redirect_to @fav_product, notice: 'Fav product was successfully updated.' }
        format.json { render :show, status: :ok, location: @fav_product }
      else
        format.html { render :edit }
        format.json { render json: @fav_product.errors, status: :unprocessable_entity }
      end
    end
  end

  def remove_fav_prod
    FavProduct.delete_all(product_id: fav_product_params , user_id: current_user.id)
    respond_to do |format|
      # format.html { redirect_to fav_products_url, notice: 'Fav product was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  # DELETE /fav_products/1
  # DELETE /fav_products/1.json
  def destroy


    @fav_product.destroy
    respond_to do |format|
      format.html { redirect_to fav_products_url, notice: 'Fav product was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_fav_product
      @fav_product = FavProduct.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def fav_product_params
      params.require(:product_id)
    end
end
