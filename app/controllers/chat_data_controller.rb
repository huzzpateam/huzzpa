class ChatDataController < ApplicationController
  # before_action :set_chat_datum, only: [:show, :edit, :update, :destroy]
  before_filter :authenticate_user!
  layout false, only: [:get_chat_images, :get_user_profile ]

  # GET /chat_data
  # GET /chat_data.json
  def index

    @status = Status.all
    if current_user.role == "user"
      @chat_access_allowed = ChatAccess.where(user_id: current_user.id  , status: true)
      @messages = Message.where(image_present: true )
      @hash_vendor_id_conv_id = {}

      if @chat_access_allowed.present?
        @chat_access_allowed.each do |t|
          if Conversation.where(sender_id: current_user.id , recipient_id: t.vendor_profile.vendor.id).present? || Conversation.where(sender_id: t.vendor_profile.vendor.id , recipient_id: current_user.id).present?
            if Conversation.where(sender_id: current_user.id , recipient_id: t.vendor_profile.vendor.id).present?
              @conversations_id = Conversation.where(sender_id: current_user.id , recipient_id: t.vendor_profile.vendor.id).pluck(:id)[0]
            else
              @conversations_id = Conversation.where(sender_id: t.vendor_profile.vendor.id , recipient_id: current_user.id).pluck(:id)[0]
            end
            @hash_vendor_id_conv_id[t.vendor_profile.vendor.id] = @conversations_id
          end
        end
      end
      if  @hash_vendor_id_conv_id.present?
          @first_conv_id = @hash_vendor_id_conv_id.first[1]
      end


    elsif current_user.role == "vendor"
      @id = VendorProfile.where(vendor_id: current_user.id).pluck(:id)[0]
      @chat_access_allowed = ChatAccess.where(vendor_profile_id: @id  , status: true)
      @messages = Message.where(image_present: true )
      @hash_vendor_id_conv_id = {}

      if @chat_access_allowed.present?
        @chat_access_allowed.each do |t|
          if Conversation.where(sender_id: current_user.id , recipient_id: t.user_id).present? || Conversation.where(sender_id: t.user_id , recipient_id: current_user.id).present?
            if Conversation.where(sender_id: current_user.id , recipient_id: t.user_id).present?
              @conversations_id = Conversation.where(sender_id: current_user.id , recipient_id: t.user_id).pluck(:id)[0]
            else
              @conversations_id = Conversation.where(sender_id: t.user_id , recipient_id: current_user.id).pluck(:id)[0]
            end
            @hash_vendor_id_conv_id[t.user_id] = @conversations_id
          end
        end
      end

      if  @hash_vendor_id_conv_id.present?
          @first_conv_id = @hash_vendor_id_conv_id.first[1]
      end

    end


    if !@chat_access_allowed.present?
        flash[:notice] = "Chat Not Requested!!"
    end

  end

  def get_chat_images
    # byebug
  conv_id = 0
  if  Conversation.where(sender_id: current_user.id , recipient_id: get_chat_image_params[:user_id]).present?
    conv_id =  Conversation.where(sender_id: current_user.id , recipient_id: get_chat_image_params[:user_id])[0].id
  elsif  Conversation.where(sender_id: get_chat_image_params[:user_id] , recipient_id: current_user.id).present?
    conv_id = Conversation.where(sender_id: get_chat_image_params[:user_id] , recipient_id: current_user.id)[0].id
  end


   message_id = Message.where(image_type:  get_chat_image_params[:image_type] , image_present: true , conversation_id: conv_id ).pluck(:id)
   @chat_message_id = Message.where( id: message_id )
   @chat_images = Image.where(message_id: @chat_message_id)
   respond_to do |format|
    format.html { if @chat_images.present?
                    flash[:notice] = ""
                  else
                   flash[:notice] = "No Images Available!!"
                 end
                }
    # format.json { render :json =>  @chat_images}
   end
  end

  def get_user_profile

    @user_interest = UserInterest.where(user_id: get_user_id)
    @ques = @user_interest.uniq.pluck(:question_id)
    @questions =  Question.find(@ques)
    @user_detail = UserDetail.where(user_id: get_user_id)
    @fav_products = FavProduct.where(user_id: get_user_id)

    respond_to do |format|
     format.html
    end
  end

  def get_project_phase


    project_phase = "NA"
    project_phase_id = 0

    if  Conversation.where(sender_id: current_user.id , recipient_id: get_user_id).present?
      project_phase =  Conversation.where(sender_id: current_user.id , recipient_id: get_user_id )[0].status.name
      project_phase_id =  Conversation.where(sender_id: current_user.id , recipient_id: get_user_id )[0].status.id
    elsif  Conversation.where(sender_id: get_user_id , recipient_id: current_user.id).present?
      project_phase = Conversation.where(sender_id: get_user_id , recipient_id: current_user.id)[0].status.name
      project_phase_id = Conversation.where(sender_id: get_user_id , recipient_id: current_user.id)[0].status.id
    end

    respond_to do |format|
      # format.json { render :json => project_phase }
      format.json do
        render(json: {
          project_phase: project_phase , project_phase_id: project_phase_id
        })
      end
    end

  end

  def update_conv_status
    #code

    Conversation.find(get_covId_statusId_params[:id]).update(status_id: get_covId_statusId_params[:status_id])
    # flash[:notice] = "Chat Status Updated successfully created."
    if request.xhr?
          render :json => {:notice => "Chat Status Updated successfully."}
    end
  end
  # GET /chat_data/1
  # GET /chat_data/1.json
  def show
  end

  # GET /chat_data/new
  def new
    @chat_datum = ChatDatum.new
  end

  # GET /chat_data/1/edit
  def edit
  end

  # POST /chat_data
  # POST /chat_data.json
  def create
    @chat_datum = ChatDatum.new(chat_datum_params)

    respond_to do |format|
      if @chat_datum.save
        format.html { redirect_to @chat_datum, notice: 'Chat datum was successfully created.' }
        format.json { render :show, status: :created, location: @chat_datum }
      else
        format.html { render :new }
        format.json { render json: @chat_datum.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /chat_data/1
  # PATCH/PUT /chat_data/1.json
  def update
    respond_to do |format|
      if @chat_datum.update(chat_datum_params)
        format.html { redirect_to @chat_datum, notice: 'Chat datum was successfully updated.' }
        format.json { render :show, status: :ok, location: @chat_datum }
      else
        format.html { render :edit }
        format.json { render json: @chat_datum.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /chat_data/1
  # DELETE /chat_data/1.json
  def destroy
    @chat_datum.destroy
    respond_to do |format|
      format.html { redirect_to chat_data_url, notice: 'Chat datum was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_chat_datum
      @chat_datum = ChatDatum.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def chat_datum_params
      params.require(:chat_datum).permit(:id)
    end

    def get_chat_image_params
      params.permit(:user_id , :image_type)
    end

    def get_user_id
       params.require(:user_id)
    end

    def get_covId_statusId_params
      params.permit(:id,:status_id)
    end

end
