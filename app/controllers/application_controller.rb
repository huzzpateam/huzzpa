# Application controller
class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_filter :chat_box
  before_action :configure_permitted_parameters, if: :devise_controller?


  def chat_box
   if user_signed_in?
      if current_user.role =="user"
        @allowed_chat_vendors = ChatAccess.where(user: current_user.id , status: true).pluck(:vendor_profile_id)

      end
      if current_user.role =="vendor"
        @vndId = VendorProfile.where(vendor_id: current_user.id).pluck(:id)
        @allowed_chat_user =  ChatAccess.where(vendor_profile_id: @vndId , status: true ).pluck(:user_id)
        @allUser = User.all
      end
      @convid_sendid_recid = Conversation.all.pluck(:id , :sender_id , :recipient_id)
    end
  end

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.for(:sign_up) << [:role, :name, :phone]
    devise_parameter_sanitizer.for(:account_update) << [:role, :name, :phone, :guest]
  end

  def authorize_admin
    redirect_to root_path unless current_user.role == 'admin'
  end

  def authorize_vendor
    redirect_to root_path unless current_user.role == 'vendor'
  end

  def authorize_user
    redirect_to root_path unless current_user.role == 'user'
  end

  def authorize_only_user
    if current_user.role == 'user'  && current_user.guest
      redirect_to "/guests/#{current_user.id}/edit"
    end
  end

  def authorize_guest
    redirect_to root_path unless current_user.guest == true
  end

  def authorize_vendor_activation
    redirect_to activate_vendors_path unless current_user.activate == true
  end
end
