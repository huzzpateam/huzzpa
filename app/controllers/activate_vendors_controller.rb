class ActivateVendorsController < ApplicationController
  before_filter :authenticate_user!
  before_filter :authorize_vendor

  def index
    redirect_to vendor_profile_path(current_user.vendor_profile) if current_user.activate == true
  end

  def contact_admin
    AdminMailer.delay.activate_me(current_user)
    flash[:notice] = "Hey #{current_user.name}, We have sent an email on behalf of you to Admin
    for an account activation. Enjoy!!"
    redirect_to activate_vendors_path
  end
end
