# Vendor Profile Logic.
class VendorProfilesController < ApplicationController
  before_action :set_vendor_profile, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!, except: [:public_profile]
  before_action :authorize_vendor, except: [:public_profile]
  before_action :authorized_vendor, only: [:show, :edit, :update, :destroy]
  before_action :authorize_vendor_activation, except: [:public_profile]

  # GET /vendor_profiles
  # GET /vendor_profiles.json
  # def index
  #   @vendor_profiles = VendorProfile.all
  # end

  # GET /vendor_profiles/1
  # GET /vendor_profiles/1.json
  def show
  end

  # GET /vendor_profiles/new
  def new
    @vendor_profile = VendorProfile.new
  end

  # GET /vendor_profiles/1/edit
  def edit
  end

  # POST /vendor_profiles
  # POST /vendor_profiles.json
  def create
    @vendor_profile = current_user.build_vendor_profile(vendor_profile_params)
    # @vendor_profile

    respond_to do |format|
      if @vendor_profile.save
        format.html { redirect_to @vendor_profile, notice: 'Vendor profile was successfully created.' }
        format.json { render :show, status: :created, location: @vendor_profile }
      else
        format.html { render :new }
        format.json { render json: @vendor_profile.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /vendor_profiles/1
  # PATCH/PUT /vendor_profiles/1.json
  def update
    respond_to do |format|
      if @vendor_profile.update(vendor_profile_params)
        format.html { redirect_to @vendor_profile, notice: 'Vendor profile was successfully updated.' }
        format.json { render :show, status: :ok, location: @vendor_profile }
      else
        format.html { render :edit }
        format.json { render json: @vendor_profile.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /vendor_profiles/1
  # DELETE /vendor_profiles/1.json
  def destroy
    @vendor_profile.destroy
    respond_to do |format|
      format.html { redirect_to vendor_profiles_url, notice: 'Vendor profile was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def public_profile
    unless user_signed_in? && current_user.guest == true
      @vendor_id = params[:id]
      @vendor_details = User.includes(:vendor_works, :vendor_profile).where(id: @vendor_id).first
    else
      redirect_to edit_guest_path(current_user, vendor_id: params[:id])
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_vendor_profile
    @vendor_profile = VendorProfile.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def vendor_profile_params
    params.require(:vendor_profile).permit(:company, :about, :address, :city, :vendor_id, :profile_image, :website, :experience, :pincode, :price_range)
  end

  def authorized_vendor
    redirect_to root_path, notice: 'You are not authorize to make this change' unless current_user == @vendor_profile.vendor
  end
end
