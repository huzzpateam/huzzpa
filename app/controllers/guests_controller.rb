class GuestsController < ApplicationController
  before_action :set_guest, only: [:edit, :update]
  before_action :authenticate_user!, except: [:create]
  before_action :authorize_guest, except: [:create]
  before_action :authorized_guest, only: [:edit, :update]

  def create
    if !user_signed_in?
      u = User.new(name: "guest", email: "guest_#{Time.now.to_i}#{rand(100)}@example.com", role: "user", guest: true)
      u.skip_confirmation!
      u.save!(validate: false)
      sign_in(:user, u)
      user = UserProfile.where(user: u).first
      @user = UserProfile.create(user: u) unless user
      redirect_to preferences_path
    elsif user_signed_in? && current_user.guest
      redirect_to preferences_path, notice: 'Please select your preferences!!'
    else
      redirect_to root_path
    end
  end

  def edit
  end

  def update
    guest = current_user.guest ? true : false
    # should we ask user to confirm email once they update their email id.
    if @user.update(guest_params)
      sign_in(:user, @user, bypass: true) # As in general flow Devise logs out the user when password is changed, hence we are
                                          # manually signing in the User and bypassing the warden callback.
      AdminMailer.delay.new_user(@user) if guest == true
      WelcomeMailer.delay.welcome_new_user(@user)
      # redirect_to public_vendor_profiles_path(id: params[:vendor_id])
      redirect_to matched_vendors_path
    else
     render :edit
    end
  end

  private

  def set_guest
    @user = User.find(params[:id])
  end

  def guest_params
    params.require(:user).permit(:name, :email, :phone, :password, :password_confirmation, :guest)
  end

  def authorized_guest
    redirect_to root_path, notice: 'You are not authorize to make this change' unless current_user == @user
  end
end
