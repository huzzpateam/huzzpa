# Category items related additional methods
module CategoryItemsRelated
  # Create a new category item
  def self.create(category_item_params, params)
    item = CategoryItem.new(category_item_params)
    item = item_body(item, params)
    item
  end

  # Item body
  def self.item_body(item, params)
    item.tags = params[:category_item][:tags].to_s.split(/,\s*/)
    type = params[:category_item][:type_id]
    return item if type == ''
    item.type_id = type
    item.type_id = item.category.name if type.to_s == ''
    item
  end
end
