# Vendor Preferenes related additional methods
module VendorPreferencesRelated
  # Create a new category item
  def self.create(params, current_user)
    # Only create new item if its less or equal to 10.
    checked = params[:checked]
    return true if checked == 'false' && VendorPreference.find_by(
      vendor: current_user, category_item_id: params[:category_item_id],
      category_id: params[:category_id], preference_no: nil).destroy
    return false if checked == 'true' && VendorPreference.where(
      vendor: current_user, category_id: params[:category_id],
      preference_no: nil).size == 10
    return true if VendorPreference.find_or_create_by(
      vendor: current_user, category_item_id: params[:category_item_id],
      category_id: params[:category_id], preference_no: nil)
  end

  # Finish selection process
  def self.finish(current_user)
    no = current_user.vendor_profile.preference_no.to_i + 1
    selection = current_user.vendor_profile.selection_no.to_i + 1
    selected = VendorPreference.where(vendor: current_user, preference_no: nil)
    return selected if selected.size == 0 || selection > 3
    current_user.vendor_profile.update(preference_no: no, selection_no: selection)
    selected.update_all(preference_no: no)
    selected
  end
end
