# User Preferenes related additional methods
module UserPreferencesRelated
  # Create a new category item
  def self.create(params, current_user)
    checked = params[:checked]
    return true if checked == 'false' && UserPreference.find_by(
      user: current_user, category_item_id: params[:category_item_id],
      category_id: params[:category_id], preference_no: nil).destroy
    # return false if checked == 'true' && UserPreference.where(
    #   user: current_user, category_id: params[:category_id],
    #   preference_no: nil).size == 10
    return true if checked == 'true' && UserPreference.find_or_create_by(
      user: current_user, category_item_id: params[:category_item_id],
      category_id: params[:category_id], preference_no: nil)
  end

  # Finish selection process
  def self.finish(current_user)
    no = current_user.user_profile.preference_no.to_i + 1
    selected = UserPreference.where(user: current_user, preference_no: nil)
    return selected if selected.size == 0 # || no > 3
    current_user.user_profile.update(preference_no: no)
    selected.update_all(preference_no: no)
    selected
  end
end
