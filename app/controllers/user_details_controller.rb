class UserDetailsController < ApplicationController
  before_action :set_user_detail, only: [:show, :edit, :update, :destroy]

  # GET /user_details
  # GET /user_details.json
  def index
    @user_details = UserDetail.all
  end

  # GET /user_details/1
  # GET /user_details/1.json
  def show
  end

  # GET /user_details/new
  def new
    @user_detail = UserDetail.new
  end

  # GET /user_details/1/edit
  def edit
  end

  # POST /user_details
  # POST /user_details.json
  def create
    @user_detail = UserDetail.new(user_detail_params)
    if  UserDetail.where(user_id: current_user.id).present?
       id =  UserDetail.where(user_id: current_user.id).first.id
       if FlatImage.where(user_detail_id: id).present?
        #  Remove all dependent Images before removing record
         FlatImage.where(user_detail_id: id).delete_all
       end
      UserDetail.where(user_id: current_user.id).delete_all
    end

    @user_profile = UserProfile.where(user_id: current_user.id).pluck(:preference_no)
    @preference_no = 1
    if @user_profile[0].present?
      @preference_no = @user_profile[0].next
    end
    UserProfile.where(user_id: current_user.id).update_all(preference_no: @preference_no)

    respond_to do |format|
      if @user_detail.save
        # format.html { redirect_to matched_vendors_path, notice: 'User detail was successfully created.' }
        format.html { redirect_to '/user_preferences/get_interest_questions' }
        format.json { render :show, status: :created, location: matched_vendors_path }
      else
        format.html { render :new }
        format.json { render json: @user_detail.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /user_details/1
  # PATCH/PUT /user_details/1.json
  def update
    respond_to do |format|
      if @user_detail.update(user_detail_params)
        format.html { redirect_to @user_detail, notice: 'User detail was successfully updated.' }
        format.json { render :show, status: :ok, location: @user_detail }
      else
        format.html { render :edit }
        format.json { render json: @user_detail.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /user_details/1
  # DELETE /user_details/1.json
  def destroy
    @user_detail.destroy
    respond_to do |format|
      format.html { redirect_to user_details_url, notice: 'User detail was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user_detail
      @user_detail = UserDetail.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_detail_params
      params.require(:user_detail).permit(:floor_plan_image  , :user_id , :budget , :description , :work_scope => [] , :spaces => [] , flat_images_files: [])
    end
end
