# OVerriding the Devise Rgistration Controller
class RegistrationsController < Devise::RegistrationsController

  def create
    super
   AdminMailer.delay.new_user(resource) unless resource.invalid?
   create_profile(resource)
  end

  protected

  # def update_resource(resource, params)
  #   guest = current_user.guest ? true : false
  #   resource.update_attributes (params)
  #   resource.update_without_password (params)
  #   # AdminMailer.new_user(resource).deliver_now if guest == true
  # end
  def update_resource(resource, params)
    if current_user.provider == "facebook" || current_user.provider == "google_oauth2"
      params.delete("current_password")
      resource.update_without_password(params)
    else
      resource.update_with_password(params)
    end
  end

  def create_profile(resource)
    if resource.role == 'vendor'
      @vendor_profile = VendorProfile.find_or_create_by(vendor: resource, company: 'I work for?', about: 'Something interesting about me', address: 'I stay at?',
                                        website: 'whats your online address?')
      # if !@vendor_profile.save
      #   AdminMailer.error(resource).deliver_now
      # end
    elsif resource.role == 'user'
      @user = UserProfile.find_or_create_by(user: resource, location: 'Location of the work', pincode: 0000, work_details: 'Something about the work')
      # if !@user.save
      #   AdminMailer.error(resource).deliver_now
      # end
    end
  end

  def after_update_path_for(resource)
    if resource.role == 'vendor'
      vendor_profile_path(resource.vendor_profile)
    elsif resource.role == 'user'
      user_profile_path(resource.user_profile)
    elsif resource.role == 'admin'
      root_path
    end
  end

  def after_sign_up_path_for(resource)
    if resource.role == 'vendor'
      vendor_profile_path(resource.vendor_profile)
    elsif resource.role == 'user'
      # user_profile_path(resource.user_profile)
      preferences_path
    end
  end

  # def after_inactive_sign_up_path_for(resource_or_scope)
  #   session["user_return_to"] || root_path
  # end
end
