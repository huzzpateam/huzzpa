class CommentsController < ApplicationController

  def index

  end

  def new
     @comment = Comment.new(parent_id: params[:parent_id])
  end

  def reply
    @comment = Comment.find(get_comment_id_params[:id])
    render :layout => false
  end

  def create


    if params[:comment][:parent_id].to_i > 0
     parent = Comment.find_by_id(params[:comment].delete(:parent_id))
     @comment = parent.children.build(comment_params)
   else
     @comment = Comment.new(comment_params)
   end

    @comment.user = current_user

   if @comment.save
     respond_to do |format|
       if  request.xhr?  == 0
        format.json { render json: @comment }
       else
        format.html { redirect_to :back }
     end

     end
   else
     render 'new'
   end
  end

  def destroy
    if get_comment.user == current_user || current_user.role == "admin"
      @comment.destroy
      respond_to do |format|
        format.html { redirect_to :back, notice: 'Comment  successfully removed.' }
        format.json { head :no_content }
      end
    else
      respond_to do |format|
        format.html { redirect_to :back, notice: 'You are Not Authorized!!' }
        format.json { head :no_content }
      end
    end

  end

  private
    def comment_params
      params.require(:comment).permit(:content, :user_id, :blog_id)
    end
    def get_comment
      @comment = Comment.find(params[:id])
    end
    def get_comment_id_params
        params.require(:comment)
    end
end
