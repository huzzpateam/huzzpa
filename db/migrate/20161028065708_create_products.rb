class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string :name
      t.string :product_image_id
      t.integer :price
      t.string :description
      t.string :seller
      t.belongs_to :category, index: true, foreign_key: true
      t.text :tags, array: true

      t.timestamps null: false
    end
  end
end
