class CreateProjectImages < ActiveRecord::Migration
  def change
    create_table :project_images do |t|
      t.string :file_id
      t.references :huzzpa_work, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
