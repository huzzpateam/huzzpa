class CreateBlogs < ActiveRecord::Migration
  def change
    create_table :blogs do |t|
      t.string :title
      t.string :author
      t.text :body
      t.string :image_id

      t.timestamps null: false
    end
  end
end
