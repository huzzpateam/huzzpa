class CreateUserProfiles < ActiveRecord::Migration
  def change
    create_table :user_profiles do |t|
      t.text :location
      t.text :work_details
      t.text :profile_image_id
      t.integer :preference_no
      t.integer :user_id, references: 'user'

      t.timestamps null: false
    end
  end
end
