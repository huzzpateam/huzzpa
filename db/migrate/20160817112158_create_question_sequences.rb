class CreateQuestionSequences < ActiveRecord::Migration
  def change
    create_table :question_sequences do |t|
      t.belongs_to :question, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
