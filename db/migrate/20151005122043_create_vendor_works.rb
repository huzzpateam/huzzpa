class CreateVendorWorks < ActiveRecord::Migration
  def change
    create_table :vendor_works do |t|
      t.string :image_id
      t.text :description
      t.integer :price
      t.integer :vendor_id, references: 'user'

      t.timestamps null: false
    end
  end
end
