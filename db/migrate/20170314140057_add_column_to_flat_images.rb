class AddColumnToFlatImages < ActiveRecord::Migration
  def change
    add_column :flat_images, :file_filename, :string
    add_column :flat_images, :file_size, :integer
    add_column :flat_images, :file_content_type, :string
  end
end
