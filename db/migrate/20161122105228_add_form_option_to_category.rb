class AddFormOptionToCategory < ActiveRecord::Migration
  def change
      add_column :categories, :form_option, :boolean
  end
end
