class RenameProfileImageFromVendorProfiles < ActiveRecord::Migration
  def change
    remove_column :vendor_profiles, :profile_image
    add_column :vendor_profiles, :profile_image_id, :integer
  end
end
