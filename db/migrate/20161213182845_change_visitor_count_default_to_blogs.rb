class ChangeVisitorCountDefaultToBlogs < ActiveRecord::Migration
  def change
    change_column :blogs, :visitor_count, :integer, :default => 0
  end
end
