class CreateFlatImages < ActiveRecord::Migration
  def change
    create_table :flat_images do |t|
      t.string :file_id
      t.references :user_detail, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
