class AddStatusToChatAccess < ActiveRecord::Migration
  def change
  	add_column :chat_accesses, :status , :boolean, default: false
  end
end
