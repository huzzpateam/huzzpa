class AddWebsiteAndExperienceToVendorProfiles < ActiveRecord::Migration
  def change
    add_column :vendor_profiles, :website, :string
    add_column :vendor_profiles, :experience, :integer
  end
end
