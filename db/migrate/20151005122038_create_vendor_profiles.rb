class CreateVendorProfiles < ActiveRecord::Migration
  def change
    create_table :vendor_profiles do |t|
      t.string :company
      t.text :about
      t.text :address
      t.integer :vendor_id, references: 'user'

      t.timestamps null: false
    end
    add_index :vendor_profiles, :vendor_id
  end
end
