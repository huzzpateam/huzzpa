class AddImageImagepresentToMessages < ActiveRecord::Migration
  def change
    add_column :messages, :image_present, :boolean
    add_column :messages, :image_id, :string
  end
end
