class AddProfileImageToVendorProfiles < ActiveRecord::Migration
  def change
    add_column :vendor_profiles, :profile_image, :string
  end
end
