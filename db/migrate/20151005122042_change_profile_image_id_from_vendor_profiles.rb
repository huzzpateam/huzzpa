class ChangeProfileImageIdFromVendorProfiles < ActiveRecord::Migration
  def change
    change_column :vendor_profiles, :profile_image_id, :string
  end
end
