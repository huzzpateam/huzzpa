class AddSpacesDescriptionToUserDetail < ActiveRecord::Migration
  def change
  	add_column :user_details, :spaces, :string, array: true, default: []
  	 add_column :user_details, :description, :string
  end
end
