class CreateVendorPortfolioImages < ActiveRecord::Migration
  def change
    create_table :vendor_portfolio_images do |t|
      t.string :file_id
      t.references :vendor_work, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
