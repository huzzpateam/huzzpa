class CreateChatAccesses < ActiveRecord::Migration
  def change
    create_table :chat_accesses do |t|
      t.belongs_to :user, index: true, foreign_key: true
      t.belongs_to :vendor_profile, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
