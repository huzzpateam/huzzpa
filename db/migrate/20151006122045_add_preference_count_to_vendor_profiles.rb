class AddPreferenceCountToVendorProfiles < ActiveRecord::Migration
  def change
    add_column :vendor_profiles, :preference_no, :integer
  end
end
