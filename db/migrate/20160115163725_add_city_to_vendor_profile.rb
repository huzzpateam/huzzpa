class AddCityToVendorProfile < ActiveRecord::Migration
  def change
    add_column :vendor_profiles, :city, :string
  end
end
