class AddPincodeAndPriceRangeToVendorProfiles < ActiveRecord::Migration
  def change
    add_column :vendor_profiles, :pincode, :integer
    add_column :vendor_profiles, :price_range, :integer
  end
end
