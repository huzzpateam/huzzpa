class CreateVendorPreferences < ActiveRecord::Migration
  def change
    create_table :vendor_preferences do |t|
      t.integer :vendor_id, references: 'user'
      t.references :category_item, index: true, foreign_key: true
      t.references :category, index: true, foreign_key: true
      t.integer :price_from
      t.integer :price_to

      t.timestamps null: false
    end
  end
end
