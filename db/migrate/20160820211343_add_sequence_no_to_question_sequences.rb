class AddSequenceNoToQuestionSequences < ActiveRecord::Migration
  def change
    add_column :question_sequences, :sequence_no, :integer
  end
end
