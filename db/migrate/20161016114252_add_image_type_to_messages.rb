class AddImageTypeToMessages < ActiveRecord::Migration
  def change
    add_column :messages, :image_type, :string
  end
end
