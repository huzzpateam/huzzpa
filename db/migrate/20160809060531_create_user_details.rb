class CreateUserDetails < ActiveRecord::Migration
  def change
    create_table :user_details do |t|
      t.string :floor_plan_image_id
      t.string :flat_image_id
      t.integer :budget
      t.text :work_scope, array: true
      t.belongs_to :user, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
