class AddCategoryAndVisitorCountToBlogs < ActiveRecord::Migration
  def change
    add_reference :blogs, :category, index: true, foreign_key: true
    add_column :blogs, :visitor_count, :integer
  end
end
