class AddImageTypeToUserDetailsFloorPlanImages < ActiveRecord::Migration
  def change
    add_column :user_details, :floor_plan_image_filename, :string
    add_column :user_details, :floor_plan_image_size, :integer
    add_column :user_details, :floor_plan_image_content_type, :string
  end
end
