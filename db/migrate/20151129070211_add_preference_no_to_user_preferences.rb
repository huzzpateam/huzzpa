class AddPreferenceNoToUserPreferences < ActiveRecord::Migration
  def change
    add_column :user_preferences, :preference_no, :integer
  end
end
