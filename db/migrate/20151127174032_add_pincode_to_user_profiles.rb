class AddPincodeToUserProfiles < ActiveRecord::Migration
  def change
    add_column :user_profiles, :pincode, :integer
  end
end
