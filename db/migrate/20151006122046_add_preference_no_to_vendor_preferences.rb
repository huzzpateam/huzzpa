class AddPreferenceNoToVendorPreferences < ActiveRecord::Migration
  def change
    add_column :vendor_preferences, :preference_no, :integer
  end
end
