class AddTypeToBlogs < ActiveRecord::Migration
  def change
    add_reference :blogs, :type, index: true, foreign_key: true
  end
end
