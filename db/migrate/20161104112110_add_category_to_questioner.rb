class AddCategoryToQuestioner < ActiveRecord::Migration
  def change
    add_reference :questioners, :category, index: true, foreign_key: true
  end
end
