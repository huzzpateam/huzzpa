class AddProfileImageToCategories < ActiveRecord::Migration
  def change
    add_column :categories, :profile_image_id, :string
  end
end
