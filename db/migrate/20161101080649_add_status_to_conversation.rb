class AddStatusToConversation < ActiveRecord::Migration
  def change
    # add_reference :conversations, :status, index: true, foreign_key: true, default: 1
    add_reference :conversations, :status, index: true, foreign_key: true
  end
end
