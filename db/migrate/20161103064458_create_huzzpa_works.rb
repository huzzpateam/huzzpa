class CreateHuzzpaWorks < ActiveRecord::Migration
  def change
    create_table :huzzpa_works do |t|
      t.string :logo_id
      t.string :image_id
      t.string :name
      t.string :location
      t.text :description

      t.timestamps null: false
    end
  end
end
