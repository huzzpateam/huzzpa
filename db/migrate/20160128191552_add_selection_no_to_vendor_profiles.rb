class AddSelectionNoToVendorProfiles < ActiveRecord::Migration
  def change
    add_column :vendor_profiles, :selection_no, :integer
  end
end
