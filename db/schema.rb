# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170314145814) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "blogs", force: :cascade do |t|
    t.string   "title"
    t.string   "author"
    t.text     "body"
    t.string   "image_id"
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.integer  "category_id"
    t.integer  "visitor_count", default: 0
    t.integer  "type_id"
    t.text     "subtitle"
    t.string   "slug"
  end

  add_index "blogs", ["category_id"], name: "index_blogs_on_category_id", using: :btree
  add_index "blogs", ["slug"], name: "index_blogs_on_slug", unique: true, using: :btree
  add_index "blogs", ["type_id"], name: "index_blogs_on_type_id", using: :btree

  create_table "categories", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.string   "profile_image_id"
    t.boolean  "form_option"
  end

  create_table "category_items", force: :cascade do |t|
    t.string   "image_id"
    t.text     "description"
    t.text     "tags",                     array: true
    t.integer  "type_id"
    t.integer  "category_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "category_items", ["category_id"], name: "index_category_items_on_category_id", using: :btree
  add_index "category_items", ["type_id"], name: "index_category_items_on_type_id", using: :btree

  create_table "chat_accesses", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "vendor_profile_id"
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
    t.boolean  "status",            default: false
  end

  add_index "chat_accesses", ["user_id"], name: "index_chat_accesses_on_user_id", using: :btree
  add_index "chat_accesses", ["vendor_profile_id"], name: "index_chat_accesses_on_vendor_profile_id", using: :btree

  create_table "ckeditor_assets", force: :cascade do |t|
    t.string   "data_id",                      null: false
    t.string   "data_filename",                null: false
    t.integer  "data_size"
    t.string   "data_content_type"
    t.integer  "assetable_id"
    t.string   "assetable_type",    limit: 30
    t.string   "type",              limit: 30
    t.integer  "width"
    t.integer  "height"
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
  end

  add_index "ckeditor_assets", ["assetable_type", "assetable_id"], name: "idx_ckeditor_assetable", using: :btree
  add_index "ckeditor_assets", ["assetable_type", "type", "assetable_id"], name: "idx_ckeditor_assetable_type", using: :btree

  create_table "comment_hierarchies", id: false, force: :cascade do |t|
    t.integer "ancestor_id",   null: false
    t.integer "descendant_id", null: false
    t.integer "generations",   null: false
  end

  add_index "comment_hierarchies", ["ancestor_id", "descendant_id", "generations"], name: "comment_anc_desc_udx", unique: true, using: :btree
  add_index "comment_hierarchies", ["descendant_id"], name: "comment_desc_idx", using: :btree

  create_table "comments", force: :cascade do |t|
    t.text     "content"
    t.integer  "blog_id"
    t.integer  "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "parent_id"
  end

  add_index "comments", ["blog_id"], name: "index_comments_on_blog_id", using: :btree
  add_index "comments", ["user_id"], name: "index_comments_on_user_id", using: :btree

  create_table "conversations", force: :cascade do |t|
    t.integer  "sender_id"
    t.integer  "recipient_id"
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.integer  "status_id",    default: 1
  end

  add_index "conversations", ["recipient_id"], name: "index_conversations_on_recipient_id", using: :btree
  add_index "conversations", ["sender_id"], name: "index_conversations_on_sender_id", using: :btree
  add_index "conversations", ["status_id"], name: "index_conversations_on_status_id", using: :btree

  create_table "delayed_jobs", force: :cascade do |t|
    t.integer  "priority",   default: 0, null: false
    t.integer  "attempts",   default: 0, null: false
    t.text     "handler",                null: false
    t.text     "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string   "locked_by"
    t.string   "queue"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "delayed_jobs", ["priority", "run_at"], name: "delayed_jobs_priority", using: :btree

  create_table "fav_products", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "product_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "fav_products", ["product_id"], name: "index_fav_products_on_product_id", using: :btree
  add_index "fav_products", ["user_id"], name: "index_fav_products_on_user_id", using: :btree

  create_table "flat_images", force: :cascade do |t|
    t.string   "file_id"
    t.integer  "user_detail_id"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.string   "file_filename"
    t.integer  "file_size"
    t.string   "file_content_type"
  end

  add_index "flat_images", ["user_detail_id"], name: "index_flat_images_on_user_detail_id", using: :btree

  create_table "friendly_id_slugs", force: :cascade do |t|
    t.string   "slug",                      null: false
    t.integer  "sluggable_id",              null: false
    t.string   "sluggable_type", limit: 50
    t.string   "scope"
    t.datetime "created_at"
  end

  add_index "friendly_id_slugs", ["slug", "sluggable_type", "scope"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type_and_scope", unique: true, using: :btree
  add_index "friendly_id_slugs", ["slug", "sluggable_type"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type", using: :btree
  add_index "friendly_id_slugs", ["sluggable_id"], name: "index_friendly_id_slugs_on_sluggable_id", using: :btree
  add_index "friendly_id_slugs", ["sluggable_type"], name: "index_friendly_id_slugs_on_sluggable_type", using: :btree

  create_table "huzzpa_works", force: :cascade do |t|
    t.string   "logo_id"
    t.string   "image_id"
    t.string   "name"
    t.string   "location"
    t.text     "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "images", force: :cascade do |t|
    t.string   "file_id"
    t.integer  "message_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "images", ["message_id"], name: "index_images_on_message_id", using: :btree

  create_table "messages", force: :cascade do |t|
    t.text     "body"
    t.integer  "conversation_id"
    t.integer  "user_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.boolean  "image_present"
    t.string   "image_id"
    t.string   "image_type"
  end

  add_index "messages", ["conversation_id"], name: "index_messages_on_conversation_id", using: :btree
  add_index "messages", ["user_id"], name: "index_messages_on_user_id", using: :btree

  create_table "products", force: :cascade do |t|
    t.string   "name"
    t.string   "product_image_id"
    t.integer  "price"
    t.string   "description"
    t.string   "seller"
    t.integer  "category_id"
    t.text     "tags",                          array: true
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  add_index "products", ["category_id"], name: "index_products_on_category_id", using: :btree

  create_table "project_images", force: :cascade do |t|
    t.string   "file_id"
    t.integer  "huzzpa_work_id"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.string   "file_filename"
    t.integer  "file_size"
    t.string   "file_content_type"
  end

  add_index "project_images", ["huzzpa_work_id"], name: "index_project_images_on_huzzpa_work_id", using: :btree

  create_table "question_sequences", force: :cascade do |t|
    t.integer  "question_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.integer  "sequence_no"
    t.integer  "category_id"
  end

  add_index "question_sequences", ["category_id"], name: "index_question_sequences_on_category_id", using: :btree
  add_index "question_sequences", ["question_id"], name: "index_question_sequences_on_question_id", using: :btree

  create_table "questioners", force: :cascade do |t|
    t.integer  "category_item_id"
    t.integer  "question_id"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.integer  "category_id"
  end

  add_index "questioners", ["category_id"], name: "index_questioners_on_category_id", using: :btree
  add_index "questioners", ["category_item_id"], name: "index_questioners_on_category_item_id", using: :btree
  add_index "questioners", ["question_id"], name: "index_questioners_on_question_id", using: :btree

  create_table "questions", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "answer"
  end

  create_table "statuses", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "tags", force: :cascade do |t|
    t.string   "name"
    t.integer  "category_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "tags", ["category_id"], name: "index_tags_on_category_id", using: :btree

  create_table "types", force: :cascade do |t|
    t.string   "name"
    t.integer  "category_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "types", ["category_id"], name: "index_types_on_category_id", using: :btree

  create_table "user_details", force: :cascade do |t|
    t.string   "floor_plan_image_id"
    t.string   "flat_image_id"
    t.integer  "budget"
    t.text     "work_scope",                                              array: true
    t.integer  "user_id"
    t.datetime "created_at",                                 null: false
    t.datetime "updated_at",                                 null: false
    t.string   "spaces",                        default: [],              array: true
    t.string   "description"
    t.string   "floor_plan_image_filename"
    t.integer  "floor_plan_image_size"
    t.string   "floor_plan_image_content_type"
  end

  add_index "user_details", ["user_id"], name: "index_user_details_on_user_id", using: :btree

  create_table "user_interests", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "question_id"
    t.integer  "category_item_id"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  add_index "user_interests", ["category_item_id"], name: "index_user_interests_on_category_item_id", using: :btree
  add_index "user_interests", ["question_id"], name: "index_user_interests_on_question_id", using: :btree
  add_index "user_interests", ["user_id"], name: "index_user_interests_on_user_id", using: :btree

  create_table "user_preferences", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "category_id"
    t.integer  "category_item_id"
    t.integer  "price_from"
    t.integer  "price_to"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.integer  "preference_no"
  end

  add_index "user_preferences", ["category_id"], name: "index_user_preferences_on_category_id", using: :btree
  add_index "user_preferences", ["category_item_id"], name: "index_user_preferences_on_category_item_id", using: :btree
  add_index "user_preferences", ["user_id"], name: "index_user_preferences_on_user_id", using: :btree

  create_table "user_profiles", force: :cascade do |t|
    t.text     "location"
    t.text     "work_details"
    t.text     "profile_image_id"
    t.integer  "preference_no"
    t.integer  "user_id"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.integer  "pincode"
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "",    null: false
    t.string   "encrypted_password",     default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.string   "role"
    t.string   "name"
    t.string   "phone"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.boolean  "guest",                  default: false
    t.string   "provider"
    t.string   "uid"
    t.string   "unconfirmed_email"
    t.boolean  "activate"
  end

  add_index "users", ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true, using: :btree
  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["provider"], name: "index_users_on_provider", using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  add_index "users", ["uid"], name: "index_users_on_uid", using: :btree

  create_table "vendor_portfolio_images", force: :cascade do |t|
    t.string   "file_id"
    t.integer  "vendor_work_id"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  add_index "vendor_portfolio_images", ["vendor_work_id"], name: "index_vendor_portfolio_images_on_vendor_work_id", using: :btree

  create_table "vendor_preferences", force: :cascade do |t|
    t.integer  "vendor_id"
    t.integer  "category_item_id"
    t.integer  "category_id"
    t.integer  "price_from"
    t.integer  "price_to"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.integer  "preference_no"
  end

  add_index "vendor_preferences", ["category_id"], name: "index_vendor_preferences_on_category_id", using: :btree
  add_index "vendor_preferences", ["category_item_id"], name: "index_vendor_preferences_on_category_item_id", using: :btree

  create_table "vendor_profiles", force: :cascade do |t|
    t.string   "company"
    t.text     "about"
    t.text     "address"
    t.integer  "vendor_id"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.string   "profile_image_id"
    t.integer  "preference_no"
    t.string   "website"
    t.integer  "experience"
    t.integer  "pincode"
    t.integer  "price_range"
    t.string   "city"
    t.integer  "selection_no"
  end

  add_index "vendor_profiles", ["vendor_id"], name: "index_vendor_profiles_on_vendor_id", using: :btree

  create_table "vendor_works", force: :cascade do |t|
    t.string   "image_id"
    t.text     "description"
    t.integer  "price"
    t.integer  "vendor_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_foreign_key "blogs", "categories"
  add_foreign_key "blogs", "types"
  add_foreign_key "category_items", "categories"
  add_foreign_key "category_items", "types"
  add_foreign_key "chat_accesses", "users"
  add_foreign_key "chat_accesses", "vendor_profiles"
  add_foreign_key "comments", "blogs"
  add_foreign_key "comments", "users"
  add_foreign_key "conversations", "statuses"
  add_foreign_key "fav_products", "products"
  add_foreign_key "fav_products", "users"
  add_foreign_key "flat_images", "user_details"
  add_foreign_key "images", "messages"
  add_foreign_key "messages", "conversations"
  add_foreign_key "messages", "users"
  add_foreign_key "products", "categories"
  add_foreign_key "project_images", "huzzpa_works"
  add_foreign_key "question_sequences", "categories"
  add_foreign_key "question_sequences", "questions"
  add_foreign_key "questioners", "categories"
  add_foreign_key "questioners", "category_items"
  add_foreign_key "questioners", "questions"
  add_foreign_key "tags", "categories"
  add_foreign_key "types", "categories"
  add_foreign_key "user_details", "users"
  add_foreign_key "user_interests", "category_items"
  add_foreign_key "user_interests", "questions"
  add_foreign_key "user_interests", "users"
  add_foreign_key "user_preferences", "categories"
  add_foreign_key "user_preferences", "category_items"
  add_foreign_key "user_preferences", "users"
  add_foreign_key "vendor_portfolio_images", "vendor_works"
  add_foreign_key "vendor_preferences", "categories"
  add_foreign_key "vendor_preferences", "category_items"
end
