namespace :import_blogs do
   task :task_one => :environment do
    #  files = Dir.glob("/home/soumitra/Documents/WorkspaceFoofys/makeuber/public/blogs/**")
     files = Dir.glob("/home/deploy/hutfy/current/public/blogs/**")
     puts "Reached directory"
     files.each do |file|
      fork do
        file = File.open(file).read
        doc = Nokogiri::HTML(file)
        # Fetch Title
        title = doc.xpath("/html/body/article/section[2]/section/div[2]/div/h3").text
        # remove the section of title
        doc.xpath("/html/body/article/section[2]/section/div[2]/div/h3").remove
        # Fetch Sub Title
        subtitle = doc.xpath("/html/body/article/section[2]/section/div[2]/div/p[1]").text
        #Remove subtitile section
        doc.xpath("/html/body/article/section[2]/section/div[2]/div/p[1]").remove
        # Author
        author = "Huzzpa Home Stories"
        #Remove Horizontal line
        doc.xpath("/html/body/article/section[2]/section/div[1]/hr").remove
        #Remove footer
        doc.xpath("/html/body/div[1]/div[1]/div[3]/article/footer").remove
        doc.xpath("/html/body/article/footer").remove
        content = doc
        puts title
        #Enter new data to blog
        blog = Blog.new
        blog.title = title
        blog.subtitle = subtitle
        blog.author = author
        blog.body = content
        blog.save!
        puts "done!"
      end
     end
   end

   task :task_two => :environment do
    #  files = Dir.glob("/home/soumitra/Documents/WorkspaceFoofys/makeuber/public/blogs/**")
     files = Dir.glob("/home/deploy/hutfy/current/public/blogs2/**")
     puts "Reached directory"
     files.each do |file|
      fork do
        file = File.open(file).read
        doc = Nokogiri::HTML(file)
        # Fetch Title
        title = doc.xpath("/html/body/article/section[2]/section/div[2]/div/h3").text
        # remove the section of title
        doc.xpath("/html/body/article/section[2]/section/div[2]/div/h3").remove
        # Fetch Sub Title
        subtitle = doc.xpath("/html/body/article/section[2]/section/div[2]/div/p[1]").text
        #Remove subtitile section
        doc.xpath("/html/body/article/section[2]/section/div[2]/div/p[1]").remove
        # Author
        author = "Huzzpa Home Stories"
        #Remove Horizontal line
        doc.xpath("/html/body/article/section[2]/section/div[1]/hr").remove
        #Remove footer
        doc.xpath("/html/body/div[1]/div[1]/div[3]/article/footer").remove
        doc.xpath("/html/body/article/footer").remove
        content = doc
        puts title
        #Enter new data to blog
        blog = Blog.new
        blog.title = title
        blog.subtitle = subtitle
        blog.author = author
        blog.body = content
        blog.save!
        puts "done!"
      end
     end
   end
end
