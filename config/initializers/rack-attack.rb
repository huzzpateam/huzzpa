class Rack::Attack

  Rack::Attack.safelist('allow from localhost & server') do |req|
  # Requests are allowed if the return value is truthy
  '127.0.0.1' == req.ip || '::1' == req.ip || '122.166.130.230' == req.ip
  end

  Rack::Attack.blocklist("Block Referrer Analytics Spam") do |request|
    spammerList = ENV.has_key?("spammerList") ? ENV["spammerList"].split(',') : []
    spammerList.find { |spammer| request.referer =~ %r{#{spammer}} }
  end

  # Rack::Attack.blacklist('bad_login_ip') do |req|
  #   if (req.post? && req.path == "/users/sign_in" && IPCat.datacenter?(req.ip))
  #     req.ip
  #   end
  # end
  Rack::Attack.blocklist('fail2ban pentesters') do |req|
  # `filter` returns truthy value if request fails, or if it's from a previously banned IP
  # so the request is blocked
    Rack::Attack::Fail2Ban.filter("pentesters-#{req.ip}", :maxretry => 1, :findtime => 10.minutes, :bantime => 1.year) do
      # The count for the IP is incremented if the return value is truthy
      req.path.include?('azenv.php') ||
      req.path.include?('.php') ||
      req.path.include?('manager')
    end
  end

  # Rack::Attack.blacklist('Stupid IP for PHP') do |req|
  #   req.path == '/azenv.php' || req.path == '/testproxy.php' || req.path == '//web/scripts/setup.php'
  # end

  # Rack::Attack.throttle('req/ip', limit: 300, period: 5.minutes) do |req|
  #   req.remote_ip if ['/assets', '/check'].any? {|path| req.path.starts_with? path }
  # end

  # Rack::Attack.throttle('req/ip', limit: 60, period: 1.minutes) do |req|
  #   if req.path == '/' && req.post?
  #     req.ip
  #   end
  # end
  # Throttle attempts to a particular path. 2 POSTs to /login per second per IP
  Rack::Attack.throttle "logins/ip", :limit => 2, :period => 1 do |req|
    req.post? && req.path == "/users/sign_in" && req.ip
  end

    # Rack::Attack.throttle('req/ip', :limit => 5, :period => 20.seconds) do |req|
    #   if req.path == '/users/sign_in' && req.post?
    #     req.ip
    #   end
    # end

  Rack::Attack.throttle('logins/email', :limit => 6, :period => 60.seconds) do |req|
    req.params['email'] if req.path == '/users/sign_in' && req.post?
  end
end
