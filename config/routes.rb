Rails.application.routes.draw do

  mount Ckeditor::Engine => '/ckeditor'
  resources :chat_data do
   collection do
    get 'get_chat_images'
    get 'get_user_profile'
    get 'update_conv_status'
    get 'get_project_phase'
   end
 end

 resources :tags
 resources :products
 resources :fav_products do
   collection do
    #  get "/fav_products/remove_fav_prod/:product_id" => "fav_products#remove_fav_prod"
    get 'remove_fav_prod'
   end
 end
 resources :huzzpa_works

 resources :blogs do
   collection do
     get 'get_blogby_category'
     get 'get_latest_blogs'
     get 'get_blogby_type'
     get 'admin_blog'
     get 'template'
     # Skip 404 and redirect
    #  get '103' => redirect('blogs')
    #  get '104' => redirect('blogs')
    #  get '5' => redirect('blogs')
    #  get '3' => redirect('blogs')
    #  get '102' => redirect('blogs')
    #  get '94' => redirect('blogs')
    #  get '2' => redirect('blogs')
    #  get '105' => redirect('blogs')
    #  get '90' => redirect('blogs')
    #  get '1' => redirect('blogs')
    #  get '5' => redirect('blogs')
   end
 end

 resources :comments, only: [:index, :create , :destroy]
 get '/comments/new/:parent_id', to: 'comments#new', as: :new_comment
 get '/comments/reply', to: 'comments#reply'


  get 'design/modular-kitchen-designers', to: 'design_inspiration#kitchen'
  get 'design-inspiration/kitchen' => redirect('design/modular-kitchen-designers')

  get 'design/modular-wardrobe-designers', to: 'design_inspiration#wardrobes'
  get 'design-inspiration/bed-room' => redirect('design/modular-wardrobe-designers')
  get 'design-inspiration/bed' => redirect('design/modular-wardrobe-designers')
  get 'design-inspiration/wardrobes' => redirect('design/modular-wardrobe-designers')

  get 'design/shoe-cabinet-designers', to: 'design_inspiration#shoe_cabinet'
  get 'design-inspiration/shoe-cabinet' => redirect('design/shoe-cabinet-designers')

  get 'design/study-unit-designers', to: 'design_inspiration#study'
  get 'design-inspiration/study-unit' => redirect('design/study-unit-designers')
  get 'design-inspiration/study' => redirect('design/study-unit-designers')

  get 'design/vanity-unit-designers', to: 'design_inspiration#vanity_unit'
  get 'design-inspiration/bath-room' => redirect('design/vanity-unit-designers')
  get 'design-inspiration/vanity-unit' => redirect('design/vanity-unit-designers')

  get 'design/tv-unit-designers', to: 'design_inspiration#tv_unit'
  get 'design-inspiration/living-room' => redirect('design/tv-unit-designers')
  get 'design-inspiration/tv-unit' => redirect('design/tv-unit-designers')

  get 'design/crockery-unit-designers', to: 'design_inspiration#crockery_unit'
  get 'design-inspiration/dining' => redirect('design/crockery-unit-designers')
  get 'design-inspiration/crockery-unit' => redirect('design/crockery-unit-designers')

  # Skip 404 and redirect
  get 'design-inspiration/wall-treatment' => redirect('/')
  get 'design-inspiration/kids-bedroom' => redirect('/')
  get 'design-inspiration/balcony' => redirect('/')



  get '/questioners/get_category_item/:category_id',      to: 'questioners#get_category_item'

  get '/user_preferences/get_question/:current_seq_no',      to: 'user_preferences#get_question'

  get '/user_preferences/get_interest_questions',      to: 'user_preferences#get_interest_questions'

  get '/user_preferences/get_next_interest_question',      to: 'user_preferences#get_next_interest_question'

  get '/user_preferences/update_user_interest',      to: 'user_preferences#update_user_interest'

  get '/user_preferences/get_client_requirement',      to: 'user_preferences#get_client_requirement'

  post "question_sequences/activate_questions" => "question_sequences#activate_questions"

  post "question_sequences/deactivate_questions" => "question_sequences#deactivate_questions"

  get "question_sequences/update_sequence" => "question_sequences#update_sequence"

  get "chat_accesses/update_status" => "chat_accesses#update_status"

  get"matched_vendors/chat_request" => "matched_vendors@chat_request"

  get 'matched_vendors/:tab' => 'matched_vendors#index' , as: 'matched_recommendations'



  resources :admins do
    collection do
      get 'vendors'
      get 'status'
      get 'new_vendor'
      post 'create_vendor'
      get 'users'
      get 'unblock'
      get 'guests'
      get 'export_users'
      get 'export_vendors'
      get 'get_user_profile'
    end
  end

  resources :users, only: [] do
    collection do
      resources :preferences, only: [:index, :create], controller: 'user_preferences' do
        collection do
          get 'select'
          get 'selected'
        end
      end
    end
  end
  #
  # resources :vendors, only: [] do
  #   collection do
  resources :vendor_preferences, only: [:index, :create] do
    collection do
      get 'select'
      get 'selected'
    end
  end

  resources :vendor_works do
    collection do
      get 'portfolio_images'
      get 'all'
    end
  end

  resources :matched_vendors do
    collection do
      post 'connect'
      post 'get_product_filter_by_categ'
    end
  end

  resources :matched_users do
    collection do
      post 'connect'
    end
  end



  resources :user_profiles, except: [:new]
  resources :vendor_profiles, except: [:new] do
    collection do
      get 'public/:id', to: 'vendor_profiles#public_profile', as: 'public'
    end
  end
  resources :types, except: [:show]
  resources :category_items, except: [:show]
  resources :categories
  resources :questions
  resources :questioners
  resources :user_interests
  resources :user_details
  resources :questioners
  resources :question_sequences
  resources :chat_accesses

  devise_for :users, controllers: {registrations: :registrations, sessions: "sessions",
                      omniauth_callbacks: 'omniauth_callbacks'}

  resources :guests

  # get 'home', to: 'home#index'
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  root 'home#index'
  get 'about', to: 'home#about'
  get 'interior_designers_mumbai', to: 'home#designer_mumbai' , as: 'interior-designers-mumbai' ,  path: "interior-designers-mumbai"
  # Skip 404 and redirect
  get 'mumbai' => redirect('interior-designers-mumbai')
  get 'interior_designers_mumbai' => redirect('interior-designers-mumbai')

  # match '/interior-designers-mumbai' => 'home#designer_mumbai'
  get 'our-portfolio', to: 'home#profile'
  get 'profile' => redirect('our-portfolio')
  post 'contact', to: 'home#contact_us'
  get 'privacy', to: 'home#privacy'
  get 'terms', to: 'home#terms'
  get 'faq', to: 'home#faq'
  get 'user-role', to: 'home#user_role'
  get 'vendor-role', to: 'home#vendor_role'
  get 'contact-us', to: 'home#contact'
  get 'contact_us' => redirect('contact-us')
  post 'subscribe', to: 'home#subscribe', as: :subscribe

  resources :activate_vendors do
    collection do
      get 'contact_admin'
    end
  end

  resources :conversations do
    resources :messages
  end

  # match "*path" => 'errors#not_found', via: :all

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
