require 'test_helper'

class QuestionSequencesControllerTest < ActionController::TestCase
  setup do
    @question_sequence = question_sequences(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:question_sequences)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create question_sequence" do
    assert_difference('QuestionSequence.count') do
      post :create, question_sequence: {  }
    end

    assert_redirected_to question_sequence_path(assigns(:question_sequence))
  end

  test "should show question_sequence" do
    get :show, id: @question_sequence
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @question_sequence
    assert_response :success
  end

  test "should update question_sequence" do
    patch :update, id: @question_sequence, question_sequence: {  }
    assert_redirected_to question_sequence_path(assigns(:question_sequence))
  end

  test "should destroy question_sequence" do
    assert_difference('QuestionSequence.count', -1) do
      delete :destroy, id: @question_sequence
    end

    assert_redirected_to question_sequences_path
  end
end
