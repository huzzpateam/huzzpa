require 'test_helper'

class HuzzpaWorksControllerTest < ActionController::TestCase
  setup do
    @huzzpa_work = huzzpa_works(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:huzzpa_works)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create huzzpa_work" do
    assert_difference('HuzzpaWork.count') do
      post :create, huzzpa_work: {  }
    end

    assert_redirected_to huzzpa_work_path(assigns(:huzzpa_work))
  end

  test "should show huzzpa_work" do
    get :show, id: @huzzpa_work
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @huzzpa_work
    assert_response :success
  end

  test "should update huzzpa_work" do
    patch :update, id: @huzzpa_work, huzzpa_work: {  }
    assert_redirected_to huzzpa_work_path(assigns(:huzzpa_work))
  end

  test "should destroy huzzpa_work" do
    assert_difference('HuzzpaWork.count', -1) do
      delete :destroy, id: @huzzpa_work
    end

    assert_redirected_to huzzpa_works_path
  end
end
