require 'test_helper'

class ChatAccessesControllerTest < ActionController::TestCase
  setup do
    @chat_access = chat_accesses(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:chat_accesses)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create chat_access" do
    assert_difference('ChatAccess.count') do
      post :create, chat_access: {  }
    end

    assert_redirected_to chat_access_path(assigns(:chat_access))
  end

  test "should show chat_access" do
    get :show, id: @chat_access
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @chat_access
    assert_response :success
  end

  test "should update chat_access" do
    patch :update, id: @chat_access, chat_access: {  }
    assert_redirected_to chat_access_path(assigns(:chat_access))
  end

  test "should destroy chat_access" do
    assert_difference('ChatAccess.count', -1) do
      delete :destroy, id: @chat_access
    end

    assert_redirected_to chat_accesses_path
  end
end
