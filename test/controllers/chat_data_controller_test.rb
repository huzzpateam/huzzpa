require 'test_helper'

class ChatDataControllerTest < ActionController::TestCase
  setup do
    @chat_datum = chat_data(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:chat_data)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create chat_datum" do
    assert_difference('ChatDatum.count') do
      post :create, chat_datum: { id: @chat_datum.id }
    end

    assert_redirected_to chat_datum_path(assigns(:chat_datum))
  end

  test "should show chat_datum" do
    get :show, id: @chat_datum
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @chat_datum
    assert_response :success
  end

  test "should update chat_datum" do
    patch :update, id: @chat_datum, chat_datum: { id: @chat_datum.id }
    assert_redirected_to chat_datum_path(assigns(:chat_datum))
  end

  test "should destroy chat_datum" do
    assert_difference('ChatDatum.count', -1) do
      delete :destroy, id: @chat_datum
    end

    assert_redirected_to chat_data_path
  end
end
