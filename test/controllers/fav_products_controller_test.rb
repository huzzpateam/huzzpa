require 'test_helper'

class FavProductsControllerTest < ActionController::TestCase
  setup do
    @fav_product = fav_products(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:fav_products)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create fav_product" do
    assert_difference('FavProduct.count') do
      post :create, fav_product: {  }
    end

    assert_redirected_to fav_product_path(assigns(:fav_product))
  end

  test "should show fav_product" do
    get :show, id: @fav_product
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @fav_product
    assert_response :success
  end

  test "should update fav_product" do
    patch :update, id: @fav_product, fav_product: {  }
    assert_redirected_to fav_product_path(assigns(:fav_product))
  end

  test "should destroy fav_product" do
    assert_difference('FavProduct.count', -1) do
      delete :destroy, id: @fav_product
    end

    assert_redirected_to fav_products_path
  end
end
