require 'test_helper'

class DesignInspirationControllerTest < ActionController::TestCase
  test "should get kitchen" do
    get :kitchen
    assert_response :success
  end

  test "should get curious" do
    get :curious
    assert_response :success
  end

  test "should get furniture" do
    get :furniture
    assert_response :success
  end

  test "should get wall" do
    get :wall
    assert_response :success
  end

end
