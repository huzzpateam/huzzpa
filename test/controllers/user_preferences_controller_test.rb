require 'test_helper'

class UserPreferencesControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
  end

  test "should get select" do
    get :select
    assert_response :success
  end

  test "should get selected" do
    get :selected
    assert_response :success
  end

end
