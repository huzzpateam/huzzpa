require 'test_helper'

class VendorWorksControllerTest < ActionController::TestCase
  setup do
    @vendor_work = vendor_works(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:vendor_works)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create vendor_work" do
    assert_difference('VendorWork.count') do
      post :create, vendor_work: { description: @vendor_work.description, image_id: @vendor_work.image_id, price: @vendor_work.price, vendor_id: @vendor_work.vendor_id }
    end

    assert_redirected_to vendor_work_path(assigns(:vendor_work))
  end

  test "should show vendor_work" do
    get :show, id: @vendor_work
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @vendor_work
    assert_response :success
  end

  test "should update vendor_work" do
    patch :update, id: @vendor_work, vendor_work: { description: @vendor_work.description, image_id: @vendor_work.image_id, price: @vendor_work.price, vendor_id: @vendor_work.vendor_id }
    assert_redirected_to vendor_work_path(assigns(:vendor_work))
  end

  test "should destroy vendor_work" do
    assert_difference('VendorWork.count', -1) do
      delete :destroy, id: @vendor_work
    end

    assert_redirected_to vendor_works_path
  end
end
